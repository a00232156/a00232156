function confirmacion_passwords(){
  //Guardar valor que se encuentra dentro de nuestra texto box.
  let nombreUsuario = document.getElementById("nombreUser").value;
  //Guardamos ambas contrasenas.
  let contrasenaInicial = document.getElementById("New_password").value;
  let contrasenaVerify = document.getElementById("New_password_verify").value;
  //Comprobar validez de las passwords.
    if(nombreUsuario == ""){
        alert("Ingrese un nombre de usuario");
    }else if(contrasenaInicial == "" || contrasenaVerify == ""){
        alert("Rellene los campos necesarios");
    }else if(contrasenaInicial == contrasenaVerify){
    alert("Bienvenido "+nombreUsuario+"!!!");
  }else{
    alert("La contraseñas ingresadas no coinciden. Por favor, intente nuevamente.");
  }
}
function confirmacion_productos(){
    let cantidad_producto_1 = document.getElementById("NumeroManzana").value;
    let cantidad_producto_2 = document.getElementById("NumeroPlatano").value;
    let cantidad_producto_3 = document.getElementById("NumeroMango").value;
    let total = ((cantidad_producto_1  * 5) + (cantidad_producto_2 * 7)+ (cantidad_producto_3 * 9))
    let iva = total*.16;
    let totaliva = total + iva;
    let div = document.getElementById("Productosalida");
    
    div.innerHTML = "<br><p>Su ticket de compra:</p><br><table> <tr> Precio base: "+total+"|  IVA: "+iva+"|   TOTAL:"+totaliva+" </tr><br> </table>"
}

function Password_segura(){
    let passwordPosible = document.getElementById("Passsegura").value;
    let div = document.getElementById("Contraseña");
    if(passwordPosible == ""){
        div.innerHTML = "<p>Introduzca una contraseña</p>";
    }else{
        let tieneMayus = false;
        let tieneNumero = false; 
        let tieneLargo = false;
        let letrasMayus = "/^[A-Z]+$/";
            if(letrasMayus.test(passwordPosible)){
                //alert("contiene mayus");
                tieneMayus = true;
            }
            if(/\d/.test(passwordPosible)){
                //alert("contiene numero");
                tieneNumero = true;
            }
            if(passwordPosible.length > 8){
                //alert("mayor que osho");
                tieneLargo = true;
            }
        console.log("Largo: "+tieneLargo);
        console.log("Numero: "+tieneNumero);
        console.log("Mayus: "+tieneMayus);
        if ( (tieneLargo == true) && (tieneNumero == true) ){
             div.innerHTML = "<p>Su contraseña tiene 8 caracteres y al menos 1 numero, es segura</p>";
        }else{
            div.innerHTML = "<p>Su contraseña debe contener 8 caracteres y al menos 1 numero, para ser segura!</p>";
        }
            
    
    }
}

//Al dar al boton de id "Confirmar" llamar a nuestra funcion "confirmacion_passwords"
document.getElementById("Confirmar").onclick = confirmacion_passwords;
document.getElementById("Passsegura").onkeyup = Password_segura;
//inicializar valores de cantidad de producto.
document.getElementById("NumeroManzana").value = 0;
document.getElementById("NumeroPlatano").value = 0;
document.getElementById("NumeroMango").value = 0;
document.getElementById("ConfirmarProductos").onclick = confirmacion_productos;
