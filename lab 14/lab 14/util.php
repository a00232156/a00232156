<?php
//función para conectarnos a la BD

function conectar_bd() {

    $conexion_bd = mysqli_connect("localhost","root","","testdb");

    if ($conexion_bd == NULL) {
        
        die("No se pudo conectar con la base de datos");
    }
    
    return $conexion_bd;
}

//funcion para cerrar la base de datos.
function cerrar_bd($mysql){
    mysqli_close($mysql);
}

function getMateriales(){
    $con = conectar_bd();
    
    $sql = "SELECT * FROM Materiales";
    
    $result = mysqli_query($con, $sql);
    
    cerrar_bd($con);
    
    return $result;
    
}
function getProyectos(){
    $con = conectar_bd();
    
    $sql = "SELECT * FROM Proyectos";
    
    $result = mysqli_query($con, $sql);
    
    cerrar_bd($con);
    
    return $result;
    
}

function getMatPrice($precio){
    $con = conectar_bd();
    
    $sql = "SELECT * FROM Materiales WHERE Costo >='".$precio."'";
    
    $result = mysqli_query($con, $sql);
    
    cerrar_bd($con);
    
    return $result;
    
}




?>