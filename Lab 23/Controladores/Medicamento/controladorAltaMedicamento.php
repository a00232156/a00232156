<?php
    session_start();
    require_once("../../util.php");
    include("Partials/General/_head.html");
    include("Partials/General/_topBar.html");
   
    include("Partials/General/_sideBar.html");
    include("Partials/General/_topBody.html");
    include("Partials/ConsultaEscolaridad/_fedback.html");
    if(isset($_POST['Registrar'])){
        $nombre = htmlspecialchars($_POST["nombre"]);
        $ingrediente = htmlspecialchars($_POST['ingrediente']);
        $presentacion = htmlspecialchars($_POST['presentacion']);

        if (nuevoMedicamento($nombre,$ingrediente,$presentacion)) {
            $_SESSION["mensaje"] = "Se ha agregado el medicamento";
        } else {
            $_SESSION["warning"] = "Ocurrió un error al agregar el medicamento";
        }
        //nuevoMedicamento($nombre,$ingrediente,$presentacion);
        include("Partials/ConsultaMedicamento/_consultaMedicamentoTitulo.html");

        include("Partials/ConsultaMedicamento/_consultaMedicamento.html"); 
            showQueryMedicamentos(getMedicamentos());
    }
    include("Partials/General/_endBody.html");
    include("Partials/General/_endPage.html");
?>