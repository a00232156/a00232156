<?php
    require_once("../../util.php");
    include("Partials/General/_head.html");
    include("Partials/General/_topBar.html");
    include("Partials/General/_sideBar.html");
    include("Partials/General/_topBody.html");
    include("Partials/ConsultaMedicamento/_consultaMedicamentoTituloR.html");

    include("Partials/ConsultaMedicamento/_consultaMedicamento.html"); 
    if(isset($_POST['nombre'])){
        $nombre = htmlspecialchars($_POST["nombre"]);
        showQueryMedicamentos(getMedicamentosPorNombre($nombre));
    }
    else{
        showQueryMedicamentos(getMedicamentos());
    }

    
            
 

        
    
    
/*
        echo "<div class=\"row\">";
            echo "<div class=\"col s4\">";
           
            include("Partials/ConsultaMedicamento/_consultaMedicamento.html");
            echo "</div>";
            if(isset($_POST['medicamento'])){
                echo "<div class=\"col s8\">";
                include("Partials/ConsultaMedicamento/_medicamento.html");
                echo "</div>";
            }
        echo "</div>";
*/
    include("Partials/General/_endBody.html");
    include("Partials/General/_endPage.html");
?>