<?php
    require_once("util.php");
    include("Partials/General/_head.html");
    include("Partials/General/_topBar.html");
    include("Partials/General/_sideBar.html");
    include("Partials/General/_topBody.html");
    include("Partials/EliminarGradoEscolar/_eliminarGradoEscolarTitulo.html");
    $gradoEscolar_id = htmlspecialchars($_GET["gradoEscolar_id"]);
        echo getGradoEscolarById($gradoEscolar_id);
    
    include("Partials/General/_endBody.html");
    include("Partials/General/_endPage.html");
?>
