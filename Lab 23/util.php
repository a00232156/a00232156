<?php
    //función para conectarnos a la BD
/*
function conectar_bd() {

    $conexion_bd = mysqli_connect("mysql1008.mochahost.com","dawbdorg_sampl","sampl","dawbdorg_sampletext");

    if ($conexion_bd == NULL) {
        
        die("No se pudo conectar con la base de datos");
    }
    
    return $conexion_bd;
}*/
function conectar_bd() {

  $conexion_bd = mysqli_connect("localhost","root","","needed");
  if ($conexion_bd == NULL) {
      
      die("No se pudo conectar con la base de datos");
  }
  
  return $conexion_bd;
}
//funcion para cerrar la base de datos.
function cerrar_bd($mysql){
    mysqli_close($mysql);
}

function getProgramas($nombre=""){
    $con = conectar_bd();
    
    $sql = "SELECT  p.idProgramaAtencion as idP, p.nombre as NombreP,a.nombre as NombreA,fechaInicial as fechaI, fechaFinal as fechaF, p.activo as activo
    FROM ProgramaAtencion as p, Area as a  
    where p.idArea = a.idArea ";
    if($nombre != ""){
        $sql .= " AND p.nombre like '%".$nombre."%'";
    }
    $result = mysqli_query($con, $sql);
    $tabla = "";
    
    if(!(mysqli_num_rows($result) == 0)){
      $tabla .= "<table class=\"highlight centered\">";
        $tabla .= "<thead><tr><th>Id de programa</th><th>Area</th><th>Nombre</th><th>Fecha de inicio</th><th>Fecha de fin</th><th>Estado</th></tr></thead>";
        while($row = mysqli_fetch_assoc($result)){   
            $tabla .= "<tr>";
            $tabla .= "<td>". $row["idP"]. "</td>";
            $tabla .= "<td>". $row["NombreA"]. "</td>";
            $tabla .= "<td>". $row["NombreP"]. "</td>";
            $tabla .= "<td>". date('d/m/Y',strtotime($row["fechaI"])). "</td>";
            $tabla .= "<td>". date('d/m/Y',strtotime($row["fechaF"])). "</td>";
            if($row["activo"]==1){
                $tabla .= "<td>"."<i class=\"material-icons\">check</i>". "</td>";
            }else{
                $tabla .= "<td>"."<i class=\"material-icons\">block</i>". "</td>";
            }
            $tabla .= '<td><a class="waves-effect waves-light btn-small" href="consultarPrograma.php?programa_id='.$row['idP'].'">'."<i class=\"material-icons\">remove_red_eye</i>"."</a>";
            $tabla .= '<a class="waves-effect waves-light btn-small" href="editarPrograma.php?programa_id='.$row['idP'].'">'."<i class=\"material-icons\">create</i>"."</a>";
            $tabla .= '<a class="waves-effect waves-light btn-small" href="EliminarPrograma.php?programa_id='.$row['idP'].'">'."<i class=\"material-icons\">delete_forever</i>"."</a>";
            $tabla .= "</td>"; 
            $tabla .= "</tr>";
        }
        $tabla .= "</table>";
    }else{
      $tabla .= "<div class=\"row\">
      <div class=\"col s12 m12 l12\">
          <div class=\"card blue lighten-1\">
              <div class=\"card-content white-text\">
                  <span class=\"card-title\">No se encontró ningún resultado de \"".$nombre."\"</span>
              </div>
          </div>
      </div>
  </div>";
    }
    cerrar_bd($con);
    
    return $tabla;
  }


function getProgramasById($id){
    $con = conectar_bd();
    
    $sql = "SELECT  p.idProgramaAtencion as idP, p.nombre as NombreP,a.nombre as NombreA,fechaInicial as fechaI, fechaFinal as fechaF, p.objetivo as objetivo
    FROM ProgramaAtencion as p, Area as a where p.idArea = a.idArea";
    $sql .= " AND p.idProgramaAtencion = '$id'";

    $result = mysqli_query($con, $sql);
    $resultado = "";
    
    
    while($row = mysqli_fetch_assoc($result)){
        $resultado .= "<br>
        <div class=\"card teal lighten-2\">
        <div class=\"card-content teal lighten-3\">
        <div class=\"carousel carousel-slider center\" data-indicators=\"true\">
          <div class=\"carousel-item teal lighten-5 \" href=\"#one!\" >
              <h2>Eliminar programa</h2>
            <div class=\"col s12 align-center\">
                <!-- Elemento -->
                <div class=\"file-field input-field\">
                  <div class=\"col s6 m6 l6\">
                    <label>Nombre del programa</label>
                    <p>".$row["NombreP"]."</p>
                </div>
                </div>
                <div class=\"file-field input-field\">
                    <div class=\"col s6 m6 l6\">
                        <label>Area a la que pertecene el programa</label>
                        <p>".$row["NombreA"]."</p>
                    </div>
                </div>
                <br>
                <div class=\"row\">
                  <div class=\"file-field input-field\">
                    <div class=\"col s6\">
                        <label>Fecha de inicio:</label>
                        <p>".date('d/m/Y',strtotime($row["fechaI"]))."</p>
                    </div>
                    <div class=\"col s6\">
                        <label>Fecha de termino:</label>
                        <p>".date('d/m/Y',strtotime($row["fechaF"]))."</p>
                    </div>
                  </div>
                  <br>
                </div>
                <div class=\"file-field input-field\">
                    <div class=\"col s12\">
                        <label>Objetivo del programa</label>
                        <p>".$row["objetivo"]."</p>
                    </div>
                </div>
                <br>
                <br>
                <br>
                <br>
                <div class=\"file-field input-field\">
                  <div class=\"input-field col s12 l12 left\">
                    <a id=\"confirmarEliminarPrograma\" href=\"Controladores\Programa\controladorEliminarPrograma.php?programa_id=".$id."\" class=\"waves-effect red lime darken-4 btn right\">eliminar
                      <i class=\"material-icons right\">remove</i>
                    </a>
                    <a id=\"\" href=\"consultaPrograma.php\" class=\"waves-effect green lighten-3 btn right\">Volver<i class=\"material-icons right\">undo</i>
                    </a>
                </div>
                </div>
              </div>
          </div>
        </div>
        </div>
        </div>";
    }

    cerrar_bd($con);
    
    return $resultado;

}
function getProgramasByIdC($id){
    $con = conectar_bd();
    
    $sql = "SELECT  p.idProgramaAtencion as idP, p.nombre as NombreP,a.nombre as NombreA,fechaInicial as fechaI, fechaFinal as fechaF, p.objetivo as objetivo
    FROM ProgramaAtencion as p, Area as a where p.idArea = a.idArea";
    $sql .= " AND p.idProgramaAtencion = '$id'";

    $result = mysqli_query($con, $sql);
    $resultado = "";
    
    
    while($row = mysqli_fetch_assoc($result)){
        $resultado .= "<br>
        <div class=\"card teal lighten-2\">
        <div class=\"card-content teal lighten-3\">
        <div class=\"carousel carousel-slider center\" data-indicators=\"true\">
          <div class=\"carousel-item teal lighten-5 \" href=\"#one!\" >
              <h2>Programa numero #".$row["idP"]."</h2>
            <div class=\"col s12 align-center\">
                <!-- Elemento -->
                <div class=\"file-field input-field\">
                  <div class=\"col s6 m6 l6\">
                    <label>Nombre del programa</label>
                    <p>".$row["NombreP"]."</p>
                </div>
                </div>
                <div class=\"file-field input-field\">
                    <div class=\"col s6 m6 l6\">
                        <label>Area a la que pertecene el programa</label>
                        <p>".$row["NombreA"]."</p>
                    </div>
                </div>
                <br>
                <div class=\"row\">
                  <div class=\"file-field input-field\">
                    <div class=\"col s6\">
                        <label>Fecha de inicio:</label>
                        <p>".date('d/m/Y',strtotime($row["fechaI"]))."</p>
                    </div>
                    <div class=\"col s6\">
                        <label>Fecha de termino:</label>
                        <p>".date('d/m/Y',strtotime($row["fechaF"]))."</p>
                    </div>
                  </div>
                  <br>
                </div>
                <div class=\"file-field input-field\">
                    <div class=\"col s12\">
                        <label>Objetivo del programa</label>
                        <p>".$row["objetivo"]."</p>
                    </div>
                </div>
                <br>
                <br>
                <br>
                
                <br>
                <div class=\"file-field input-field\">
                  <div class=\"input-field col s12 l12 left\">
                    <a id=\"\" href=\"consultaPrograma.php\" class=\"waves-effect green lighten-3 btn right\">Volver<i class=\"material-icons right\">undo</i>
                    </a>
                </div>
                </div>
              </div>
          </div>
        </div>
        </div>
        </div>";
    }

    cerrar_bd($con);
    
    return $resultado;

}
//función para editar un registro de programa de atencion haciendolo inactivo
//@param caso_id: id del programa que se va a editar

function delProgramasById($id){
    
    $conexion_bd = conectar_bd();
      
    //Prepara la consulta
    $dml = 'UPDATE ProgramaAtencion SET activo='."0".' WHERE idprogramaatencion=(?)';
    if ( !($statement = $conexion_bd->prepare($dml)) ) {
        die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
        return 0;
    }
      
    //Unir los parámetros de la función con los parámetros de la consulta   
    //El primer argumento de bind_param es el formato de cada parámetro
    if (!$statement->bind_param("i", $id)) {
        die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }
      
    //Executar la consulta
    if (!$statement->execute()) {
      die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }

    cerrar_bd($conexion_bd);
      return 1;
  }

function selectAreas($id){
    $con = conectar_bd();
    
    $sql = "SELECT idArea as id, nombre FROM Area";
    
    $result = mysqli_query($con, $sql);
    $select = "";
    
    if(mysqli_num_rows($result)){
        $select .= "<select name=\"areas\" id=\"areas\" required >";
        if($id == ""){
            $select .= "<option value=\"\" selected >Elija un area</option>";
        }
        while($row = mysqli_fetch_assoc($result)){   
            $select .= '<option ';
            if($id != ""){
                if($id == $row["id"]){
                    $select .= ' selected ';
                }
            }
            $select .= 'value="'.$row["id"].'">'.$row["nombre"].'</option>"';
        }
        $select .= "</select>";
    }
    cerrar_bd($con);
    
    return $select;
}



function altaPrograma($idarea, $nombre, $fechaI, $fechaF, $objetivo){
    $activo = "1";
    $conexion_bd = conectar_bd();
      
    //Prepara la consulta
    $dml = 'INSERT INTO ProgramaAtencion (idArea,nombre,fechaInicial,fechaFinal,objetivo,activo) VALUES (?,?,?,?,?,?)';
    if ( !($statement = $conexion_bd->prepare($dml)) ) {
        die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
        return 0;
    }
      
    //Unir los parámetros de la función con los parámetros de la consulta   
    //El primer argumento de bind_param es el formato de cada parámetro
    if (!$statement->bind_param("ssssss",$idarea,$nombre, $fechaI, $fechaF, $objetivo, $activo)) {
        die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }
      
    //Executar la consulta
    if (!$statement->execute()) {
      die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }

    cerrar_bd($conexion_bd);
      return 1;
}

function AreasEdit($id){
    $con = conectar_bd();
    
    $sql = "SELECT  p.idArea as idA ,p.idProgramaAtencion as idP, p.nombre as NombreP,a.nombre as NombreA,fechaInicial as fechaI, fechaFinal as fechaF, p.objetivo as objetivo
    FROM ProgramaAtencion as p, Area as a where p.idArea = a.idArea";
    $sql .= " AND p.idProgramaAtencion = '$id'";

    $result = mysqli_query($con, $sql);
    $resultado = "";
    
    
    while($row = mysqli_fetch_assoc($result)){
        $resultado .= "<br>
        <div class=\"card teal lighten-2\">
        <div class=\"card-content teal lighten-3\">
        <div class=\"carousel carousel-slider center\" data-indicators=\"true\">
          <div class=\"carousel-item teal lighten-5 \" href=\"#one!\" >
              <h2>Edición de programa</h2>
            <div class=\"col s12 align-center\">
              <form action=\"Controladores\Programa\controladorEditarPrograma.php\" method=\"POST\">
                <!-- Elemento -->
                <div class=\"file-field input-field\">
                  <div class=\"input-field col s6 l6\">
                    <i class=\"material-icons prefix\"> </i>
                    <input name=\"nombre\" type=\"text\" class=\"validate\" value=\"".$row["NombreP"]."\">
                    <label for=\"rol\"><strong>Nombre del Programa</strong></label>     
                </div>
                </div>
                <div class=\"file-field input-field\">
                  <div class=\"input-field col s6 l6\">";
        $resultado .= selectAreas($row["idA"]);
        $resultado .= "<label for=\"rol\"><strong>Area del Programa</strong></label>
                        </div>
                    </div>
                    
                    <div class=\"row\">
                        <div class=\"file-field input-field\">
                        <div class=\"input-field col s6 l6\">
                            <i class=\"material-icons prefix\"> </i>
                            <input type=\"date\" name=\"fechaI\" value=\"".date('Y-m-d',strtotime($row["fechaI"]))."\">
                            <label for=\"rol\"><strong>Fecha de Inicio:</strong></label>     
                        </div>
                        </div>
                        <div class=\"file-field input-field\">
                        <div class=\"input-field col s6 l6\">
                            <i class=\"material-icons prefix\"> </i>
                            <input type=\"date\" name=\"fechaF\" value=\"".date('Y-m-d',strtotime($row["fechaF"]))."\">
                            <label for=\"rol\"><strong>Fecha de Termino:</strong></label>     
                        </div>
                        </div>
                        </div>
                    <div class=\"file-field input-field\">
                        <div class=\"input-field col s12 l12\">
                        <i class=\"material-icons prefix\"> </i>
                        <textarea type=\"text\" name=\"objetivo\" placeholder=\"Ej. Lograr un nivel apto de lecturas para menores de 10 años\" type=\"text\" class=\"materialize-textarea\"
                        >".$row["objetivo"]."</textarea>
                        <label for=\"rol\"><strong>Objetivo del Programa</strong></label>     
                    </div>
                    </div>
                    
                    <div class=\"file-field input-field\">
                        <div class=\"input-field col s12 l12 left\">
                        <button class=\"waves-effect waves-light btn right\" type=\"submit\" name=\"action\">Registrar
                            <i class=\"material-icons right\">add</i>
                        </button>
                        <a id=\"confirmarEliminarPrograma\" href=\"consultaPrograma.php\" class=\"waves-effect green lighten-3 btn right\">Volver<i class=\"material-icons right\">undo</i>
                    </a>
                    </div>
                    </div>
                    </form>
                    </div>
                </div>
                </div>
                </div>
                </div>";
    }

    cerrar_bd($con);
    
    return $resultado;
}

function editaPrograma($idP,$idarea, $nombre, $fechaI, $fechaF, $objetivo){
    $activo = "1";
    $conexion_bd = conectar_bd();
      
    //Prepara la consulta
    $dml = 'UPDATE  ProgramaAtencion 
    SET idArea=(?),nombre=(?),fechaInicial=(?),fechaFinal=(?),objetivo=(?),activo=(?) 
    WHERE idProgramaAtencion=(?)';
    
    if ( !($statement = $conexion_bd->prepare($dml)) ) {
        die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
        return 0;
    }
      
    //Unir los parámetros de la función con los parámetros de la consulta   
    //El primer argumento de bind_param es el formato de cada parámetro
    if (!$statement->bind_param("sssssss",$idarea,$nombre, $fechaI, $fechaF, $objetivo, $activo,$idP)) {
        die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }
      
    //Executar la consulta
    if (!$statement->execute()) {
      die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }

    cerrar_bd($conexion_bd);
      return 1;
}


// InstituciOwOnes Traer Todas las instituciones + Ajax

function getInstituciones($nombre=""){
    $con = conectar_bd();
    
    $sql = "SELECT  i.idInstitucion as idI, i.nombre as NombreI FROM Institucion as i";
    if($nombre != ""){
        $sql .= " WHERE i.nombre LIKE '%".$nombre."%'";
    }
    $result = mysqli_query($con, $sql);
    $tabla = "";
    
    if(mysqli_num_rows($result)){
        $tabla .= "<table class=\"highlight centered\">";
        $tabla .= "<thead><tr><th>Id de Institucion</th><th>Nombre</th><th>Accion</th></tr></thead>";
        while($row = mysqli_fetch_assoc($result)){   
            $tabla .= "<tr>";
            $tabla .= "<td>". $row["idI"]. "</td>";
            $tabla .= "<td>". $row["NombreI"]. "</td>";
            $tabla .= '<td><a class="waves-effect waves-light btn-small" href="consultarInstitucion.php?institucion_id='.$row['idI'].'">'."<i class=\"material-icons\">remove_red_eye</i>"."</a>";
            $tabla .= '<a class="waves-effect waves-light btn-small" href="editarInstitucion.php?institucion_id='.$row['idI'].'">'."<i class=\"material-icons\">create</i>"."</a>";
            $tabla .= '<a class="waves-effect waves-light btn-small" href="eliminarInstitucion.php?institucion_id='.$row['idI'].'">'."<i class=\"material-icons\">delete_forever</i>"."</a>";
            $tabla .= "</td>"; 
            $tabla .= "</tr>";
        }
        $tabla .= "</table>";
    }
    cerrar_bd($con);
    
    return $tabla;
}

// Instituciones .::.::::.:::. 

function getInstitucionesByIdC($id){
    $con = conectar_bd();
    
    $sql = "SELECT  i.idInstitucion as idI, i.nombre as NombreI FROM Institucion as i";
    $sql .= " WHERE i.idInstitucion = '$id'";

    $result = mysqli_query($con, $sql);
    $resultado = "";
    
    
    while($row = mysqli_fetch_assoc($result)){
        $resultado .= "<br>
        <div class=\"card teal lighten-2\">
        <div class=\"card-content teal lighten-3\">
        <div class=\"carousel carousel-slider center\" data-indicators=\"true\">
          <div class=\"carousel-item teal lighten-5 \" href=\"#one!\" id=\"carousel-Docs\">
              <h2>Institucion numero #".$row["idI"]."</h2>
            <div class=\"col s12 align-center\">
                <!-- Elemento -->
                <div class=\"file-field input-field\">
                  <div class=\"col s12 m12 l12\">
                    <label>Nombre de la Institucion</label>
                    <p>".$row["NombreI"]."</p>
                </div>
                </div>
                <br>
                <br>
                <br>
                
                <br>
                <div class=\"file-field input-field\">
                  <div class=\"input-field col s12 l12 left\">
                    <a id=\"confirmarEliminarPrograma\" href=\"consultaInstitucion.php\" class=\"waves-effect green lighten-3 btn right\">Regresar<i class=\"material-icons right\">undo</i>
                    </a>
                </div>
                </div>
              </div>
          </div>
        </div>
        </div>
        </div>";
    }

    cerrar_bd($con);
    
    return $resultado;

}

function editInstitucion($id){
    $con = conectar_bd();
    
    $sql = "SELECT  i.idInstitucion as idI, i.nombre as NombreI FROM Institucion as i";
    $sql .= " WHERE i.idInstitucion = '$id'";

    $result = mysqli_query($con, $sql);
    $resultado = "";
    
    
    while($row = mysqli_fetch_assoc($result)){
        $resultado .= "<br>
        <div class=\"card teal lighten-2\">
        <div class=\"card-content teal lighten-3\">
        <div class=\"carousel carousel-slider center\" data-indicators=\"true\">
          <div class=\"carousel-item teal lighten-5 \" href=\"#one!\"  id=\"carousel-Docs\" >
              <h2>Edición de programa</h2>
            <div class=\"col s12 align-center\">
              <form action=\"Controladores\Institucion\controladorEditarInstitucion.php\" method=\"POST\">
                <!-- Elemento -->
                <div class=\"file-field input-field\">
                  <div class=\"input-field col s6 l6\">
                    <i class=\"material-icons prefix\"> </i>
                    <input required name=\"nombre\" type=\"text\" class=\"validate\" value=\"".$row["NombreI"]."\">
                    <label for=\"rol\"><strong>Nombre de la Institucion</strong></label>     
                </div>
                </div>
                

                    
                <div class=\"file-field input-field\">
                <div class=\"input-field col s12 l12 left\">
                        <button class=\"waves-effect waves-light btn right\" type=\"submit\" name=\"action\">Registrar
                            <i class=\"material-icons right\">add</i>
                        </button>
                        <a id=\"confirmarEliminarPrograma\" href=\"consultaInstitucion.php\" class=\"waves-effect green lighten-3 btn right\">Volver<i class=\"material-icons right\">undo</i>
                    </a>
                </div>
                </div>
                </form>
                </div>
                </div>
                </div>
                </div>
                </div>";
    }

    cerrar_bd($con);
    
    return $resultado;
}

// Instituciones Modificar

function editaInstitucion($idI, $nombre){
    
    $conexion_bd = conectar_bd();
      
    //Prepara la consulta
    $dml = 'UPDATE  Institucion SET nombre=(?)  WHERE idInstitucion=(?)';
    
    if ( !($statement = $conexion_bd->prepare($dml)) ) {
        die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
        return 0;
    }
      
    //Unir los parámetros de la función con los parámetros de la consulta   
    //El primer argumento de bind_param es el formato de cada parámetro
    if (!$statement->bind_param("ss",$nombre,$idI)) {
        die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }
      
    //Executar la consulta
    if (!$statement->execute()) {
      die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }

    cerrar_bd($conexion_bd);
      return 1;
}

function getInstitucionesById($id){
    $con = conectar_bd();
    
    $sql = "SELECT  i.idInstitucion as idI, i.nombre as NombreI FROM Institucion as i";
    $sql .= " WHERE i.idInstitucion = '$id'";

    $result = mysqli_query($con, $sql);
    $resultado = "";
    
    
    while($row = mysqli_fetch_assoc($result)){
        $resultado .= "<br>
        <div class=\"card teal lighten-2\">
        <div class=\"card-content teal lighten-3\">
        <div class=\"carousel carousel-slider center\" data-indicators=\"true\">
          <div class=\"carousel-item teal lighten-5 \" href=\"#one!\" id=\"carousel-Docs\">
              <h2>Eliminar Institucion</h2>
            <div class=\"col s12 align-center\">
                <!-- Elemento -->
                <div class=\"file-field input-field\">
                  <div class=\"col s12 m12 l12\">
                    <label>Nombre de la Institucion</label>
                    <p>".$row["NombreI"]."</p>
                </div>
                </div>
                <br>
                <br>
                <br>
                <br>
                <div class=\"file-field input-field\">
                  <div class=\"input-field col s12 l12 left\">
                    <a id=\"confirmarEliminarPrograma\" href=\"Controladores\Institucion\controladorEliminarInstitucion.php?institucion_id=".$id."\" class=\"waves-effect red deep-orange darken-4 btn right\">eliminar
                      <i class=\"material-icons right\">remove</i>
                    </a>
                </div>
                </div>
              </div>
          </div>
        </div>
        </div>
        </div>";
    }

    cerrar_bd($con);
    
    return $resultado;

}

function delInstitucionById($id){
    
    $conexion_bd = conectar_bd();
      
    //Prepara la consulta
    
    $dml = 'DELETE FROM Institucion WHERE idInstitucion =(?)';
    if ( !($statement = $conexion_bd->prepare($dml)) ) {
        die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
        return 0;
    }
      
    //Unir los parámetros de la función con los parámetros de la consulta   
    //El primer argumento de bind_param es el formato de cada parámetro
    if (!$statement->bind_param("i", $id)) {
        die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }
      
    //Executar la consulta
    if (!$statement->execute()) {
      die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }
    cerrar_bd($conexion_bd);
      return 1;
  }

  function altaInstitucion($nombre){
    $conexion_bd = conectar_bd();
      
    //Prepara la consulta
    $dml = 'INSERT INTO Institucion (nombre) VALUES (?)';
    if ( !($statement = $conexion_bd->prepare($dml)) ) {
        die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
        return 0;
    }
      
    //Unir los parámetros de la función con los parámetros de la consulta   
    //El primer argumento de bind_param es el formato de cada parámetro
    if (!$statement->bind_param("s",$nombre)) {
        die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }
      
    //Executar la consulta
    if (!$statement->execute()) {
      die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }

    cerrar_bd($conexion_bd);
      return 1;
}
function getEscuelas($nombre=""){
    $con = conectar_bd();
    $sql = "SELECT  E.idEscuela as idE, E.nombre as NombreE, E.activo as activo
    FROM Escuela as E ";
    if($nombre != ""){
        $sql .= " where     E.nombre like '%".$nombre."%'";
    }
    $result = mysqli_query($con, $sql);
    $tabla = "";
    if(mysqli_num_rows($result)){
        $tabla .= "<table class=\"highlight centered\">";
        $tabla .= "<thead><tr><th>Id de escuela</th><th>Nombre</th><th>Activo</th><th></th></thead>";
        while($row = mysqli_fetch_assoc($result)){   
            $tabla .= "<tr>";
            $tabla .= "<td>". $row["idE"]. "</td>";
            $tabla .= "<td>". $row["NombreE"]. "</td>";
            if($row["activo"]==1){
              $tabla .= "<td>"."<i class=\"material-icons\">check</i>". "</td>";
            }else{
              $tabla .= "<td>"."<i class=\"material-icons\">block</i>". "</td>";
            }
            $tabla .= '<td><a class="waves-effect waves-light btn-small" href="editarEscuela.php?escuela_id='.$row['idE'].'">'."<i class=\"material-icons\">create</i>"."</a>";
            $tabla .= '<a class="waves-effect waves-light btn-small" href="eliminarEscuela.php?escuela_id='.$row['idE'].'">'."<i class=\"material-icons\">delete_forever</i>"."</a>";
            $tabla .= "</td>"; 
            $tabla .= "</tr>";
        }
        $tabla .= "</table>";
    }
    else{
      $tabla .= "
      <div class=\"row\">
      <div class=\"col s12 m12 l12\">
          <div class=\"card blue lighten-1\">
              <div class=\"card-content white-text\">
                  <span class=\"card-title\">No se encontró ningún resultado de ".$nombre."</span>
              </div>
          </div>
      </div>
  </div>
      ";
    }
    cerrar_bd($con);
    
    return $tabla;
}

function getEscuelaById($id){
    $con = conectar_bd();
    
    $sql = "SELECT  E.idEscuela as idE, E.nombre as NombreE
    FROM Escuela as E ";
    $sql .= " Where E.idEscuela = '".$id."'";

    $result = mysqli_query($con, $sql);
    $resultado = "";
    
    
    while($row = mysqli_fetch_assoc($result)){
        $resultado .= "<br>
        <div class=\"card teal lighten-2\">
        <div class=\"card-content teal lighten-3\">
        <div class=\"carousel carousel-slider center\" data-indicators=\"true\">
          <div class=\"carousel-item teal lighten-5 \" href=\"#one!\" >
              <h2>Eliminar Escuela</h2>
            <div class=\"col s12 align-center\">
                <!-- Elemento -->
                <div class=\"file-field input-field\">
                  <div class=\"col s12\">
                    <label>Nombre de la escuela</label>
                    <p>".$row["NombreE"]."</p>
                </div>
                </div>
                <br>
                <br>
                <br>
                <br>
                <div class=\"file-field input-field\">
                  <div class=\"input-field col s12 l12 left\">
                    <a id=\"confirmarEliminarEscuela\" href=\"Controladores\Escuela\controladorEliminarEscuela.php?escuela_id=$id\" class=\"waves-effect red lime darken-4 btn right\">eliminar
                      <i class=\"material-icons right\">remove</i>
                    </a>
                    <a id=\"\" href=\"consultaEscuela.php\" class=\"waves-effect green lighten-3 btn right\">Volver<i class=\"material-icons right\">undo</i>
                    </a>
                </div>
                </div>

              </div>
          </div>
        </div>
        </div>
        </div>";
    }
    cerrar_bd($con);
    
    return $resultado;

}
function textEscuela($id=""){
    $con = conectar_bd();
        $sql = "SELECT  E.idEscuela as idE, E.nombre as NombreE
        FROM Escuela as E Where E.idEscuela = '".$id."'";
        $result = mysqli_query($con, $sql);

    $resultado = "<div class=\"file-field input-field\">
                            <div class=\"input-field col s12\">
                                <i class=\"material-icons prefix\"> </i>
                                <input placeholder=\"secundaria general 1\" type=\"text\"  name=\"escuela_nombre\" id=\"escuela_nombre\"
                                ";
        while($row = mysqli_fetch_assoc($result)){      
                $resultado .= " value=\"$row[NombreE]\"";
                    
        }
    $resultado .="
                    required>
                    <label for=\"rol\"><strong>Nombre</strong></label>     
                    </div>
                    </div>";
    cerrar_bd($con);
    return $resultado;

}
function insertarEscuela($nombre) {
    $conexion_bd = conectar_bd();
    $activo=1;
    //Prepara la consulta
    $dml = 'INSERT INTO Escuela (nombre, activo) VALUES (?,?)';
    if ( !($statement = $conexion_bd->prepare($dml)) ) {
        die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
        return 0;
    }
    //Unir los parámetros de la función con los parámetros de la consulta   
    //El primer argumento de bind_param es el formato de cada parámetro
    if (!$statement->bind_param("si", $nombre,$activo)) {
        die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
        return 0;
    } 
    //Executar la consulta
    if (!$statement->execute()) {
      die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }
    cerrar_bd($conexion_bd);
      return 1;
  }


  function editarEscuela($Escuela_id, $nombre) {
    $conexion_bd = conectar_bd();
      
    //Prepara la consulta
    $dml = 'UPDATE Escuela SET nombre=(?) WHERE idEscuela=(?)';
    if ( !($statement = $conexion_bd->prepare($dml)) ) {
        die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
        return 0;
    }
      
    //Unir los parámetros de la función con los parámetros de la consulta   
    //El primer argumento de bind_param es el formato de cada parámetro
    if (!$statement->bind_param("ss", $nombre, $Escuela_id)) {
        die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }
      
    //Executar la consulta
    if (!$statement->execute()) {
      die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }

    cerrar_bd($conexion_bd);
      return 1;
  }
  function eliminarEscuela($escuela_id) {
    $conexion_bd = conectar_bd();
    //Prepara la consulta
    $dml = 'UPDATE Escuela SET Escuela.activo=0 WHERE Escuela.idEscuela=(?)';
    if ( !($statement = $conexion_bd->prepare($dml)) ) {
        die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
        return 0;
    }
      
    //Unir los parámetros de la función con los parámetros de la consulta   
    //El primer argumento de bind_param es el formato de cada parámetro
    if (!$statement->bind_param("i",$escuela_id)) {
        die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }
      
    //Executar la consulta
    if (!$statement->execute()) {
      die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }

    cerrar_bd($conexion_bd);
      return 1;
  }



  















  function getGradoEscolar($nombre=""){
    $con = conectar_bd();
    
    $sql = "SELECT  G.idGradoEscolar as idG, G.nombre as NombreG, G.activo as activo
    FROM GradoEscolar as G  ";
    if($nombre != ""){
        $sql .= " where     G.nombre like '%".$nombre."%'";
    }
    $result = mysqli_query($con, $sql);
    $tabla = "";
    if(!(mysqli_num_rows($result)==0)){
      if(mysqli_num_rows($result)){
        $tabla .= "<table class=\"highlight centered\">";
        $tabla .= "<thead><tr><th>Id de grado escolar</th><th>Nombre</th><th></th></thead>";
        while($row = mysqli_fetch_assoc($result)){   
            $tabla .= "<tr>";
            $tabla .= "<td>". $row["idG"]. "</td>";
            $tabla .= "<td>". $row["NombreG"]. "</td>";
            if($row["activo"]==1){
              $tabla .= "<td>"."<i class=\"material-icons\">check</i>". "</td>";
            }else{
              $tabla .= "<td>"."<i class=\"material-icons\">block</i>". "</td>";
            }
            $tabla .= '<td><a class="waves-effect waves-light btn-small" href="editarGradoEscolar.php?gradoEscolar_id='.$row['idG'].'">'."<i class=\"material-icons\">create</i>"."</a>";
            $tabla .= '<a class="waves-effect waves-light btn-small" href="eliminarGradoEscolar.php?gradoEscolar_id='.$row['idG'].'">'."<i class=\"material-icons\">delete_forever</i>"."</a>";
            $tabla .= "</td>"; 
            $tabla .= "</tr>";
        }
        $tabla .= "</table>";
      }
    }else{
      $tabla .= "
      <div class=\"row\">
      <div class=\"col s12 m12 l12\">
          <div class=\"card blue lighten-1\">
              <div class=\"card-content white-text\">
                  <span class=\"card-title\">No se encontró ningún resultado de ".$nombre."</span>
              </div>
          </div>
      </div>
  </div>
      ";
    }


    cerrar_bd($con);
    
    return $tabla;
}

function getGradoEscolarById($id){
    $con = conectar_bd();
    
    $sql = "SELECT  G.idGradoEscolar as idE, G.nombre as NombreG
    FROM GradoEscolar as G ";
    $sql .= " Where G.idGradoEscolar = '".$id."'";

    $result = mysqli_query($con, $sql);
    $resultado = "";
    
    
    while($row = mysqli_fetch_assoc($result)){
        $resultado .= "<br>
        <div class=\"card teal lighten-2\">
        <div class=\"card-content teal lighten-3\">
        <div class=\"carousel carousel-slider center\" data-indicators=\"true\">
          <div class=\"carousel-item teal lighten-5 \" href=\"#one!\" >
              <h2>Eliminar Grado Escolar</h2>
            <div class=\"col s12 align-center\">
                <!-- Elemento -->
                <div class=\"file-field input-field\">
                  <div class=\"col s12\">
                    <label>Nombre del grado escolar</label>
                    <p>".$row["NombreG"]."</p>
                </div>
                </div>

                <br>
                <br>
                <br>
                <br>
                <div class=\"file-field input-field\">
                  <div class=\"input-field col s12 l12 left\">
                    <a id=\"confirmarEliminarGradoEscolar\" href=\"Controladores\GradoEscolar\controladorEliminarGradoEscolar.php?gradoEscolar_id=$id\" class=\"waves-effect red lime darken-4 btn right\">eliminar
                      <i class=\"material-icons right\">remove</i>
                    </a>
                    <a id=\"\" href=\"consultaGradoEscolar.php\" class=\"waves-effect green lighten-3 btn right\">Volver<i class=\"material-icons right\">undo</i>
                    </a>
                </div>
                </div>
              </div>
          </div>
        </div>
        </div>
        </div>";
    }
    cerrar_bd($con);
    
    return $resultado;

}
function textGradoEscolar($id=""){
    $con = conectar_bd();
        $sql = "SELECT  G.idGradoEscolar as idG, G.nombre as NombreG
        FROM GradoEscolar as G Where G.idGradoEscolar = '".$id."'";
        $result = mysqli_query($con, $sql);

    $resultado = "<div class=\"file-field input-field\">
                            <div class=\"input-field col s12\">
                                <i class=\"material-icons prefix\"> </i>
                                <input placeholder=\"Primaria 1 grado\" type=\"text\"  name=\"gradoEscolar_nombre\" id=\"gradoEscolar_nombre\"
                                ";
        while($row = mysqli_fetch_assoc($result)){      
                $resultado .= " value=\"$row[NombreG]\"";
                    
        }
    $resultado .="
                    required>
                    <label for=\"rol\"><strong>Nombre</strong></label>     
                    </div>
                    </div>";
    cerrar_bd($con);
    return $resultado;

}
function insertarGradoEscolar($nombre) {
    $conexion_bd = conectar_bd();
    $activo=1;
    //Prepara la consulta
    $dml = 'INSERT INTO GradoEscolar (nombre, activo) VALUES (?,?)';
    if ( !($statement = $conexion_bd->prepare($dml)) ) {
        die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
        return 0;
    }
    //Unir los parámetros de la función con los parámetros de la consulta   
    //El primer argumento de bind_param es el formato de cada parámetro
    if (!$statement->bind_param("si", $nombre,$activo)) {
        die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
        return 0;
    } 
    //Executar la consulta
    if (!$statement->execute()) {
      die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }
    cerrar_bd($conexion_bd);
      return 1;
  }


  function editarGradoEscolar($id, $nombre) {
    $conexion_bd = conectar_bd();
      
    //Prepara la consulta
    $dml = 'UPDATE GradoEscolar SET nombre=(?) WHERE idGradoEscolar=(?)';
    if ( !($statement = $conexion_bd->prepare($dml)) ) {
        die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
        return 0;
    }
      
    //Unir los parámetros de la función con los parámetros de la consulta   
    //El primer argumento de bind_param es el formato de cada parámetro
    if (!$statement->bind_param("ss", $nombre, $id)) {
        die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }
      
    //Executar la consulta
    if (!$statement->execute()) {
      die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }

    cerrar_bd($conexion_bd);
      return 1;
  }
function eliminarGradoEscolar($escuela_id) {
    $conexion_bd = conectar_bd();
    //Prepara la consulta
    $dml = 'UPDATE GradoEscolar SET GradoEscolar.activo=0 WHERE GradoEscolar.idGradoEscolar=(?)';
    if ( !($statement = $conexion_bd->prepare($dml)) ) {
        die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
        return 0;
    }
      
    //Unir los parámetros de la función con los parámetros de la consulta   
    //El primer argumento de bind_param es el formato de cada parámetro
    if (!$statement->bind_param("i",$escuela_id)) {
        die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }
      
    //Executar la consulta
    if (!$statement->execute()) {
      die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }

    cerrar_bd($conexion_bd);
      return 1;
  }













  function getEscolaridad($nombre=""){
    $con = conectar_bd();
    
    $sql = "SELECT  
    B.idDeIngreso as beneficiaria_Id, B.nombre as beneficiaria_nombre, B.apellidoP as beneficiaria_ap , B.apellidoM as beneficiaria_am ,  
    Es.idEscolaridad as escolaridad_Id,Es.nombreTutor as escolaridad_Tutor, Es.correoElectronico as escolaridad_correo, Es.telefono as escolirad_telefono, DATE_FORMAT(Es.fechaInicio, '%d/%m/%Y') as escolaridad_FechaIn ,DATE_FORMAT(Es.fechaFin, '%d/%m/%Y')    as escolaridad_FechaFin,
    Es.activo as activo,E.nombre as escuela_nombre, E.idEscuela as escuela_id,
    G.nombre as gradoEscolar_nombre, G.idGradoEscolar as gradoEscolar_id
    from Escolaridad as Es, Escuela as E, GradoEscolar as G, Beneficiaria as B 
    where Es.idDeIngreso = B.idDeIngreso and Es.idEscuela = E.idEscuela and Es.idGradoEscolar = G.idGradoEscolar
        ";
    if($nombre != ""){
        $sql .= " and ((B.nombre like '%$nombre%') OR (B.apellidoP like '%".$nombre."%') OR (B.apellidoM like '%$nombre%'))";
    }
    $result = mysqli_query($con, $sql);
    $tabla = "";
    if(!(mysqli_num_rows($result)==0)){
      if(mysqli_num_rows($result)){
          $tabla .= "<table class=\"highlight centered\">";
          $tabla .= "<thead><tr><th>Id de escolaridad</th><th>Nombre</th> <th>Escuela</th> <th>Grado</th><th>Inicio</th><th> Estado </th> <th></th></thead>";
          while($row = mysqli_fetch_assoc($result)){   
              $tabla .= "<tr>";
              $tabla .= "<td>". $row["escolaridad_Id"]. "</td>";
              $tabla .= "<td>". $row["beneficiaria_nombre"]." ".$row["beneficiaria_ap"]." ".$row["beneficiaria_am"]." " ."</td>";
              $tabla .= "<td>". $row["escuela_nombre"]. "</td>";
              $tabla .= "<td>". $row["gradoEscolar_nombre"]. "</td>";
              $tabla .= "<td>". $row["escolaridad_FechaIn"]. "</td>";
              if($row["activo"]==1){
                $tabla .= "<td>"."<i class=\"material-icons\">check</i>". "</td>";
              }else{
                $tabla .= "<td>"."<i class=\"material-icons\">block</i>". "</td>";
              }
              $tabla .= '<td>';
              $tabla .= '<a class="waves-effect waves-light btn-small" href="consultarEscolaridadById.php?escolaridad_id='.$row['escolaridad_Id'].'">'."<i class=\"material-icons\">find_in_page</i>"."</a>";
              $tabla .= '<a class="waves-effect waves-light btn-small" href="editarEscolaridad.php?escolaridad_id='.$row['escolaridad_Id'].'&&beneficiaria_id='.$row['beneficiaria_Id'].'&&escuela_id='.$row['escuela_id'].'&&gradoEscolar_id='.$row['gradoEscolar_id'].'">'."<i class=\"material-icons\">create</i>"."</a>";
              $tabla .= '<a class="waves-effect waves-light btn-small" href="eliminarEscolaridad.php?escolaridad_id='.$row['escolaridad_Id'].'">'."<i class=\"material-icons\">delete_forever</i>"."</a>";
              $tabla .= "</td>"; 
              $tabla .= "</tr>";
          }
          $tabla .= "</table>";
      }
    }else{
      $tabla .= "
      <div class=\"row\">
      <div class=\"col s12 m12 l12\">
          <div class=\"card blue lighten-1\">
              <div class=\"card-content white-text\">
                  <span class=\"card-title\">No se encontró ningún resultado de ".$nombre."</span>
              </div>
          </div>
      </div>
  </div>
      ";
    }
    cerrar_bd($con);
    
    return $tabla;
}


















function getEscolaridadByIdC($id){
    $con = conectar_bd();
    
    $sql = "SELECT  
    B.idDeIngreso as beneficiaria_Id, B.nombre as beneficiaria_nombre, B.apellidoP as beneficiaria_ap , B.apellidoM as beneficiaria_am ,  
    Es.idEscolaridad as escolaridad_Id,Es.nombreTutor as escolaridad_Tutor, Es.correoElectronico as escolaridad_correo, Es.telefono as escolirad_telefono, DATE_FORMAT(Es.fechaInicio, '%d/%m/%Y') as escolaridad_FechaIn ,DATE_FORMAT(Es.fechaFin, '%d/%m/%Y')    as escolaridad_FechaFin,
    Es.activo as activo,E.nombre as escuela_nombre, E.idEscuela as escuela_id,
    G.nombre as gradoEscolar_nombre, G.idGradoEscolar as gradoEscolar_id
    from Escolaridad as Es, Escuela as E, GradoEscolar as G, Beneficiaria as B 
    where Es.idDeIngreso = B.idDeIngreso and Es.idEscuela = E.idEscuela and Es.idGradoEscolar = G.idGradoEscolar
        ";
    $sql .= " and Es.idEscolaridad = '".$id."'";

    $result = mysqli_query($con, $sql);
    $resultado = "";
    
    
    while($row = mysqli_fetch_assoc($result)){
        $resultado .= "<br>
        <div class=\"card teal lighten-2\">
        <div class=\"card-content teal lighten-3\">
        <div class=\"carousel carousel-slider center\" data-indicators=\"true\">
          <div class=\"carousel-item teal lighten-5 \" href=\"#one!\" >
            <div class=\"col s12 align-center\">
                <!-- Elemento -->
                <h5>Datos de la beneficiaria</h5>
                <div class=\"file-field input-field\">
                  <div class=\"col s6\">
                  <label>Id</label>
                    <p>". $row["beneficiaria_Id"]."</p>
                  </div>
                  <div class=\"col s6\">
                    <label>Beneficiaria</label>
                    <p>". $row["beneficiaria_nombre"]." ".$row["beneficiaria_ap"]." ".$row["beneficiaria_am"]." " ."</p>
                  </div>

                </div>
                <br>
                <h5>Escolaridad</h5>
                <div class=\"file-field input-field\">

                <div class=\"col s4\">
                <label>Id</label>
                  <p>". $row["escolaridad_Id"]."</p>
                </div>

                  <div class=\"col s4\">
                    <label>Escuela</label>
                    <p>". $row["escuela_nombre"]."</p>
                  </div>
                  <div class=\"col s4\">
                  <label>Grado escolar</label>
                  <p>". $row["gradoEscolar_nombre"]." </p>
                </div>
                </div>
                <div class=\"file-field input-field\">

                <div class=\"col s6\">
                <label>Inicio</label>
                  <p>". $row["escolaridad_FechaIn"]."</p>
                </div>

                  <div class=\"col s6\">
                    <label>Termino</label>
                    <p>". $row["escolaridad_FechaFin"]."</p>
                  </div>
                </div>
                <h5>Tutor</h5>
                <div class=\"file-field input-field\">
                  <div class=\"col s4\">
                    <label>Nombre</label>
                    <p>". $row["escolaridad_Tutor"]."</p>
                   </div>
                   <div class=\"col s4\">
                   <label>Correo</label>
                   <p>". $row["escolaridad_correo"]."</p>
                  </div>
                  <div class=\"col s4\">
                  <label>Telefono</label>
                  <p>". $row["escolirad_telefono"]."</p>
                 </div>
                </div>
                <br>
                <br>
                <br>
                <br>
                <div class=\"file-field input-field\">
                  <div class=\"input-field col s12 l12 left\">
                    <a id=\"\" href=\"consultaEscolaridad.php\" class=\"waves-effect green lighten-3 btn right\">Volver<i class=\"material-icons right\">undo</i>
                    </a>
                </div>
                </div>


              </div>
          </div>
        </div>
        </div>
        </div>";
    }
    cerrar_bd($con);
    
    return $resultado;

}
function getEscolaridadByIdE($id){
    $con = conectar_bd();
    
    $sql = "SELECT  
    B.idDeIngreso as beneficiaria_Id, B.nombre as beneficiaria_nombre, B.apellidoP as beneficiaria_ap , B.apellidoM as beneficiaria_am ,  
    Es.idEscolaridad as escolaridad_Id,Es.nombreTutor as escolaridad_Tutor, Es.correoElectronico as escolaridad_correo, Es.telefono as escolirad_telefono, DATE_FORMAT(Es.fechaInicio, '%d/%m/%Y') as escolaridad_FechaIn ,DATE_FORMAT(Es.fechaFin, '%d/%m/%Y')    as escolaridad_FechaFin,
    Es.activo as activo,E.nombre as escuela_nombre, E.idEscuela as escuela_id,
    G.nombre as gradoEscolar_nombre, G.idGradoEscolar as gradoEscolar_id
    from Escolaridad as Es, Escuela as E, GradoEscolar as G, Beneficiaria as B 
    where Es.idDeIngreso = B.idDeIngreso and Es.idEscuela = E.idEscuela and Es.idGradoEscolar = G.idGradoEscolar
         ";
    $sql .= " and Es.idEscolaridad = '".$id."'";

    $result = mysqli_query($con, $sql);
    $resultado = "";
    
    
    while($row = mysqli_fetch_assoc($result)){
        $resultado .= "<br>
        <div class=\"card teal lighten-2\">
        <div class=\"card-content teal lighten-3\">
        <div class=\"carousel carousel-slider center\" data-indicators=\"true\">
          <div class=\"carousel-item teal lighten-5 \" href=\"#one!\" >
            <div class=\"col s12 align-center\">
                <!-- Elemento -->
                <h5>Datos de la beneficiaria</h5>
                <div class=\"file-field input-field\">
                  <div class=\"col s6\">
                  <label>Id</label>
                    <p>". $row["beneficiaria_Id"]."</p>
                  </div>
                  <div class=\"col s6\">
                    <label>Beneficiaria</label>
                    <p>". $row["beneficiaria_nombre"]." ".$row["beneficiaria_ap"]." ".$row["beneficiaria_am"]." " ."</p>
                  </div>

                </div>
                <br>
                <h5>Escolaridad</h5>
                <div class=\"file-field input-field\">

                <div class=\"col s4\">
                <label>Id</label>
                  <p>". $row["escolaridad_Id"]."</p>
                </div>

                  <div class=\"col s4\">
                    <label>Escuela</label>
                    <p>". $row["escuela_nombre"]."</p>
                  </div>
                  <div class=\"col s4\">
                  <label>Grado escolar</label>
                  <p>". $row["gradoEscolar_nombre"]." </p>
                </div>
                </div>
                <div class=\"file-field input-field\">

                <div class=\"col s6\">
                <label>Inicio</label>
                  <p>". $row["escolaridad_FechaIn"]."</p>
                </div>

                  <div class=\"col s6\">
                    <label>Termino</label>
                    <p>". $row["escolaridad_FechaFin"]."</p>
                  </div>
                </div>
                <h5>Tutor</h5>
                <div class=\"file-field input-field\">
                  <div class=\"col s4\">
                    <label>Nombre</label>
                    <p>". $row["escolaridad_Tutor"]."</p>
                   </div>
                   <div class=\"col s4\">
                   <label>Correo</label>
                   <p>". $row["escolaridad_correo"]."</p>
                  </div>
                  <div class=\"col s4\">
                  <label>Telefono</label>
                  <p>". $row["escolirad_telefono"]."</p>
                 </div>
                </div>
                <br>

                <br>
                <br>
                <br>
                <br>
                <div class=\"file-field input-field\">
                  <div class=\"input-field col s12 l12 left\">
                    <a id=\"confirmarEliminarEscolaridad\" href=\"Controladores\Escolaridad\controladorElimarEscolaridad.php?escolaridad_id=$id\" class=\"waves-effect red lime darken-4 btn right\">eliminar
                      <i class=\"material-icons right\">remove</i>
                    </a>
                    <a id=\"\" href=\"consultaEscolaridad.php\" class=\"waves-effect green lighten-3 btn right\">Volver<i class=\"material-icons right\">undo</i>
                    </a>
                </div>
                </div>
              </div>
          </div>
        </div>
        </div>
        </div>";
    }
    cerrar_bd($con);
    
    return $resultado;

}


function eliminarEscolaridad($escolridad_id) {
    $conexion_bd = conectar_bd();
    //Prepara la consulta
    $dml = 'UPDATE Escolaridad SET Escolaridad.activo=0 WHERE Escolaridad.idEscolaridad=(?)';
    if ( !($statement = $conexion_bd->prepare($dml)) ) {
        die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
        return 0;
    }
      
    //Unir los parámetros de la función con los parámetros de la consulta   
    //El primer argumento de bind_param es el formato de cada parámetro
    if (!$statement->bind_param("i",$escolridad_id)) {
        die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }
      
    //Executar la consulta
    if (!$statement->execute()) {
      die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }
    cerrar_bd($conexion_bd);
      return 1;
  }

  function crear_select($id, $columna_descripcion, $tabla, $seleccion=0) {
    $conexion_bd = conectar_bd();  
      
    $resultado = '<div class="input-field"><select name="'.$tabla.'" required><option value="" disabled selected>Selecciona una opción</option>';
            
    $consulta = "SELECT $id, $columna_descripcion FROM $tabla";
    $resultados = $conexion_bd->query($consulta);
    while ($row = mysqli_fetch_array($resultados, MYSQLI_BOTH)) {
        $resultado .= '<option value="'.$row["$id"].'" ';
        if($seleccion == $row["$id"]) {
            $resultado .= 'selected';
        }
        $resultado .= '>'.$row["$columna_descripcion"].'</option>';
    }
        
    cerrar_bd($conexion_bd);
    $resultado .=  '</select><label>'.$tabla.'...</label></div>';
    return $resultado;
  }


  function crear_selectBeneficiaria($seleccion=0) {
    $conexion_bd = conectar_bd();  
      
    $resultado = '<div class="input-field"><select name="beneficiaria" required><option value="" disabled selected>Selecciona una opción</option>';
            
    $consulta = "SELECT 
    B.idDeIngreso as beneficiaria_Id, B.nombre as beneficiaria_nombre, B.apellidoP as beneficiaria_ap , B.apellidoM as beneficiaria_am
    from Beneficiaria as B ";
    $resultados = $conexion_bd->query($consulta);
    while ($row = mysqli_fetch_array($resultados, MYSQLI_BOTH)) {
        $resultado .= '<option value="'.$row["beneficiaria_Id"].'"';
        if($seleccion == $row["beneficiaria_Id"]) {
            $resultado .= 'selected';
        }
        $resultado .='>'.$row["beneficiaria_Id"].'.-'.$row["beneficiaria_nombre"].' '.$row["beneficiaria_ap"].' '.$row["beneficiaria_am"].'</option>';
        
    }
        
    cerrar_bd($conexion_bd);
    $resultado .=  '</select><label>'.'beneficiaria'.'...</label></div>';
    return $resultado;
  }




  function insertarEscolaridad($idIngreso,$idEscuela,$idGrado,$tutorNombre,$telefono,$correo,$fechaInicio,$fechaFin ) {
    $conexion_bd = conectar_bd();
    $activo=1;
    //Prepara la consulta
    $dml = 'INSERT INTO Escolaridad (idDeIngreso, idEscuela,idGradoEscolar,nombreTutor,telefono,fechaInicio,fechaFin,correoElectronico,activo) VALUES (?,?,?,?,?,?,?,?,?)';
    if ( !($statement = $conexion_bd->prepare($dml)) ) {
        die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
        return 0;
    }
    //Unir los parámetros de la función con los parámetros de la consulta   
    //El primer argumento de bind_param es el formato de cada parámetro
    if (!$statement->bind_param("ssssssssi", $idIngreso,$idEscuela,$idGrado,$tutorNombre,$telefono,$fechaInicio,$fechaFin,$correo,$activo)) {
        die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
        return 0;
    } 
    //Executar la consulta
    if (!$statement->execute()) {
      die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }
    cerrar_bd($conexion_bd);
      return 1;
  }


  function editarEscolaridad($id,$idIngreso,$idEscuela,$idGrado,$tutorNombre,$telefono,$correo,$fechaInicio,$fechaFin ) {
    $conexion_bd = conectar_bd();
      
    //Prepara la consulta
    $dml = 'UPDATE Escolaridad SET idDeIngreso = (?),idEscuela=(?),idGradoEscolar=(?),nombreTutor=(?),telefono=(?),fechaInicio=(?),fechaFin=(?),correoElectronico=(?) WHERE idEscolaridad=(?)';
    if ( !($statement = $conexion_bd->prepare($dml)) ) {
        die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
        return 0;
    }
      
    //Unir los parámetros de la función con los parámetros de la consulta   
    //El primer argumento de bind_param es el formato de cada parámetro
    if (!$statement->bind_param("sssssssss",$idIngreso,$idEscuela,$idGrado,$tutorNombre,$telefono,$fechaInicio,$fechaFin,$correo,$id)) {
        die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }
      
    //Executar la consulta
    if (!$statement->execute()) {
      die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }

    cerrar_bd($conexion_bd);
      return 1;
  }



  function textCamp($idCheck,$table,$id="",$campo){
    $con = conectar_bd();
        $sql = "SELECT  T.$campo as resultado
        FROM $table as T Where T.$idCheck = '".$id."'";
        $result = mysqli_query($con, $sql);

    $resultado = "<div class=\"file-field input-field\">
                            <div class=\"input-field col s12\">
                                <i class=\"material-icons prefix\"> </i>
                                <input type=\"text\"  name=\"".$table."_"."$campo\" id=\"escuela_nombre\"
                                ";
        while($row = mysqli_fetch_assoc($result)){      
                $resultado .= " value=\"$row[resultado]\"";
                    
        }
    $resultado .="
                    >
                    <label for=\"rol\"><strong>$campo</strong></label>     
                    </div>
                    </div>";
    cerrar_bd($con);
    return $resultado;
}
function emailCamp($idCheck,$table,$id="",$campo){
  $con = conectar_bd();
      $sql = "SELECT  T.$campo as resultado
      FROM $table as T Where T.$idCheck = '".$id."'";
      $result = mysqli_query($con, $sql);

  $resultado = "
                <i class=\"material-icons prefix\"> </i>
                <input type=\"email\" class=\"validate\"  name=\"".$table."_"."$campo\" id=\"".$table."_"."$campo\"
      ";
      while($row = mysqli_fetch_assoc($result)){      
              $resultado .= " value=\"$row[resultado]\"";
      }
  $resultado .="
                  >
                  <label for=\"".$table."_"."$campo\"><strong>Email</strong></label> ";
  cerrar_bd($con);
  return $resultado;
}
function telefonoCamp($idCheck,$table,$id="",$campo){
  $con = conectar_bd();
      $sql = "SELECT  T.$campo as resultado
      FROM $table as T Where T.$idCheck = '".$id."'";
      $result = mysqli_query($con, $sql);

  $resultado = "
                <i class=\"material-icons prefix\"> </i>
                <input type=\"tel\" class=\"validate\"  name=\"".$table."_"."$campo\" id=\"".$table."_"."$campo\"
      ";
      while($row = mysqli_fetch_assoc($result)){      
              $resultado .= " value=\"$row[resultado]\"";
      }
  $resultado .="
                  >
                  <label for=\"".$table."_"."$campo\"><strong>Telephone</strong></label> ";
  cerrar_bd($con);
  return $resultado;
}
function fechaCamp($idCheck,$table,$id="",$campo){
  $con = conectar_bd();
      $sql = "SELECT  DATE_FORMAT(T.$campo,'%Y-%m-%d') as resultado
      FROM $table as T Where T.$idCheck = '".$id."'";
      $result = mysqli_query($con, $sql);

  $resultado = " 
                <input type=\"date\" class=\"validate\"  name=\"".$table."_"."$campo\" id=\"".$table."_"."$campo\"
      ";
      while($row = mysqli_fetch_assoc($result)){      
              $resultado .= " value=\"$row[resultado]\"";
      }
  $resultado .="
                  >
                  <label for=\"".$table."_"."$campo\"><strong>Telephone</strong></label> ";
  cerrar_bd($con);
  return $resultado;  
}

function getMedicamentos(){
    $conexion_bd = conectar_bd();
    $sql = "SELECT m.idMedicamento as id, m.nombre,ingredienteActivo,p.nombre as presentacion  
    FROM Medicamentos as m, Presentacion as p
    WHERE m.idPresentacion = p.idPresentacion
    ORDER BY m.idMedicamento ASC";
    $result = mysqli_query($conexion_bd, $sql);
    cerrar_bd($conexion_bd);

    return $result;
}

function getMedicamentosPorNombre($nombre){
    $conexion_bd = conectar_bd();
    $sql = "SELECT m.idMedicamento as id, m.nombre,ingredienteActivo,p.nombre as presentacion  
    FROM Medicamentos as m, Presentacion as p
    WHERE m.idPresentacion = p.idPresentacion
    AND m.nombre LIKE '$nombre%'";
    $result = mysqli_query($conexion_bd, $sql);
    cerrar_bd($conexion_bd);

    return $result;
}

function nuevoMedicamento($nombre,$ingrediente,$presentacion){
    $conexion_bd = conectar_bd();
    $sql = "INSERT INTO `Medicamentos` (`nombre`,`ingredienteActivo`,`idPresentacion`) VALUES
    ('$nombre','$ingrediente','$presentacion');";
    $result = mysqli_query($conexion_bd, $sql);
    cerrar_bd($conexion_bd);

    return $result;
}

function eliminarMedicamento($id){
    $conexion_bd = conectar_bd();
    $sql = "DELETE FROM Medicamentos WHERE idMedicamento = '$id'";
    $result = mysqli_query($conexion_bd, $sql);
    cerrar_bd($conexion_bd);

    return $result;
}

function modificarMedicamento($id,$nombre,$ingrediente,$presentacion){
    $conexion_bd = conectar_bd();
    $sql = "UPDATE Medicamentos 
    SET nombre = '$nombre', ingredienteActivo='$ingrediente', idPresentacion = '$presentacion' 
    WHERE idMedicamento = '$id'";
    $result = mysqli_query($conexion_bd, $sql);
    cerrar_bd($conexion_bd);

    return $result;
}

function showQueryMedicamentos($result){
    if(mysqli_num_rows($result) > 0){
        echo '<table class=\"highlight centered\><tr>';
        echo '<th>'.'ID'.'</th>';
        echo '<th>'.'Nombre'.'</th>';
        echo '<th>'.'Ingrediente Activo'.'</th>';
        echo '<th>'.'Presentación'.'</th>';
        echo '</tr>';
        while($row = mysqli_fetch_assoc($result)){
            echo '<tr>';
            echo '<td>'.$row['id'].'</td>';
            echo '<td>'.$row['nombre'].'</td>';
            echo '<td>'.$row['ingredienteActivo'].'</td>';
            echo '<td>'.$row['presentacion'].'</td>';
            echo '<td><a onClick="M.toast({html:\'I am a toast\'})" class="waves-effect waves-light btn-small" href="modificarMedicamento.php?medicamento_id='.$row['id'].'">'."<i class=\"material-icons\">create</i>"."</a>";
            echo "</td>"; 
            echo '<td><a class="waves-effect waves-light btn-small" href="eliminarMedicamento.php?medicamento_id='.$row['id'].'">'."<i class=\"material-icons\">delete_forever</i>"."</a>";
            echo "</td>"; 
            echo '</tr>';
        }
        echo '</table>';
    }
}

function getPresentacion(){
    $conexion_bd = conectar_bd();
    $sql = "SELECT *
    FROM Presentacion";
    $result = mysqli_query($conexion_bd, $sql);
    cerrar_bd($conexion_bd);

    return $result;
}

function showQueryPresentacion($result){
    if(mysqli_num_rows($result) > 0){
        echo '<table class=\"highlight centered\><tr>';
        echo '<th>'.'ID'.'</th>';
        echo '<th>'.'Nombre'.'</th>';
        echo '</tr>';
        while($row = mysqli_fetch_assoc($result)){
            echo '<tr>';
            echo '<td>'.$row['idPresentacion'].'</td>';
            echo '<td>'.$row['nombre'].'</td>';
            echo '<td><a class="waves-effect waves-light btn-small" href="eliminarPresentacion.php?presentacion_id='.$row['idPresentacion'].'">'."<i class=\"material-icons\">delete_forever</i>"."</a>";
            echo "</td>"; 
            echo '</tr>';
        }
        echo '</table>';
    }
}

function nuevaPresentacion($nombre){
    $conexion_bd = conectar_bd();
    $sql = "INSERT INTO `Presentacion` (`nombre`) VALUES
    ('$nombre');";
    $result = mysqli_query($conexion_bd, $sql);
    cerrar_bd($conexion_bd);

    return $result;
}

function eliminarPresentacion($id){
    $conexion_bd = conectar_bd();
    $sql = "DELETE FROM Presentacion WHERE idPresentacion = '$id'";
    $result = mysqli_query($conexion_bd, $sql);
    cerrar_bd($conexion_bd);

    return $result;
}

function crear_select1($id, $columna_descripcion, $tabla, $seleccion=0) {
  $conexion_bd = conectar_bd();  
    
  $resultado = '<div class="formato1"class="input-field"><select name="'.$tabla.'" ><option value="" disabled selected>Selecciona una opción</option>';
          
  $consulta = "SELECT $id, $columna_descripcion FROM $tabla";
  $resultados = $conexion_bd->query($consulta);
  while ($row = mysqli_fetch_array($resultados, MYSQLI_BOTH)) {
      $resultado .= '<option value="'.$row["$id"].'" ';
      if($seleccion == $row["$id"]) {
          $resultado .= 'selected';
      }
      $resultado .= '>'.$row["$columna_descripcion"].'</option>';
  }
      
  cerrar_bd($conexion_bd);
  $resultado .=  '</select><label class="formato1" for="rol"><strong>Presentacion</strong></label></div>';
  return $resultado;
}













function getDiscapacidades($nombre=""){
  $con = conectar_bd();
  $sql = "SELECT  D.idDiscapacidad as idD, D.nombre as NombreD, D.activo as activo
  FROM Discapacidad as D ";
  if($nombre != ""){
      $sql .= " where     D.nombre like '%".$nombre."%'";
  }
  $result = mysqli_query($con, $sql);
  $tabla = "";
  if(mysqli_num_rows($result)){
      $tabla .= "<table class=\"highlight centered\">";
      $tabla .= "<thead><tr><th>Id de discapacidad</th><th>Nombre</th><th>Activo</th><th></th></thead>";
      while($row = mysqli_fetch_assoc($result)){   
          $tabla .= "<tr>";
          $tabla .= "<td>". $row["idD"]. "</td>";
          $tabla .= "<td>". $row["NombreD"]. "</td>";
          if($row["activo"]==1){
            $tabla .= "<td>"."<i class=\"material-icons\">check</i>". "</td>";
          }else{
            $tabla .= "<td>"."<i class=\"material-icons\">block</i>". "</td>";
          }
          $tabla .= '<td><a class="waves-effect waves-light btn-small" href="editarDiscapacidad.php?discapacidad_id='.$row['idD'].'">'."<i class=\"material-icons\">create</i>"."</a>";
          $tabla .= '<a class="waves-effect waves-light btn-small" href="eliminarDiscapacidad.php?discapacidad_id='.$row['idD'].'">'."<i class=\"material-icons\">delete_forever</i>"."</a>";
          $tabla .= "</td>"; 
          $tabla .= "</tr>";
      }
      $tabla .= "</table>";
  }
  else{
    $tabla .= "
    <div class=\"row\">
    <div class=\"col s12 m12 l12\">
        <div class=\"card blue lighten-1\">
            <div class=\"card-content white-text\">
                <span class=\"card-title\">No se encontró ningún resultado de ".$nombre."</span>
            </div>
        </div>
    </div>
</div>
    ";
  }
  cerrar_bd($con);
  
  return $tabla;
}

function getDiscapacidadById($id){
  $con = conectar_bd();
  
  $sql = "SELECT  D.idDiscapacidad as idD, D.nombre as NombreD, D.activo as activo
  FROM Discapacidad as D ";
  $sql .= " Where D.idDiscapacidad = '".$id."'";

  $result = mysqli_query($con, $sql);
  $resultado = "";
  
  
  while($row = mysqli_fetch_assoc($result)){
      $resultado .= "<br>
      <div class=\"card teal lighten-2\">
      <div class=\"card-content teal lighten-3\">
      <div class=\"carousel carousel-slider center\" data-indicators=\"true\">
        <div class=\"carousel-item teal lighten-5 \" href=\"#one!\" >
            <h2>Eliminar Discapacidad</h2>
          <div class=\"col s12 align-center\">
              <!-- Elemento -->
              <div class=\"file-field input-field\">
                <div class=\"col s12\">
                  <label>Nombre de la discapacidad</label>
                  <p>".$row["NombreD"]."</p>
              </div>
              </div>
              <br>
              <br>
              <br>
              <br>
              <div class=\"file-field input-field\">
                <div class=\"input-field col s12 l12 left\">
                  <a id=\"confirmarEliminarDiscapacidad\" href=\"Controladores\Discapacidad\controladorEliminarDiscapacidad.php?discapacidad_id=$id\" class=\"waves-effect red lime darken-4 btn right\">eliminar
                    <i class=\"material-icons right\">remove</i>
                  </a>
                  <a id=\"\" href=\"consultaDiscapacidad.php\" class=\"waves-effect green lighten-3 btn right\">Volver<i class=\"material-icons right\">undo</i>
                  </a>
              </div>
              </div>

            </div>
        </div>
      </div>
      </div>
      </div>";
  }
  cerrar_bd($con);
  
  return $resultado;

}
function textDiscapacidad($id=""){
  $con = conectar_bd();
      $sql = "SELECT  D.idDiscapacidad as idD, D.nombre as NombreD
      FROM Discapacidad as D Where D.idDiscapacidad = '".$id."'";
      $result = mysqli_query($con, $sql);

  $resultado = "<div class=\"file-field input-field\">
                          <div class=\"input-field col s12\">
                              <i class=\"material-icons prefix\"> </i>
                              <input placeholder=\"secundaria general 1\" type=\"text\"  name=\"discapacidad_nombre\" id=\"discapacidad_nombre\"
                              ";
      while($row = mysqli_fetch_assoc($result)){      
              $resultado .= " value=\"$row[NombreD]\"";
                  
      }
  $resultado .="
                  required>
                  <label for=\"rol\"><strong>Nombre</strong></label>     
                  </div>
                  </div>";
  cerrar_bd($con);
  return $resultado;

}
function insertarDiscapacidad($nombre) {
  $conexion_bd = conectar_bd();
  $activo=1;
  //Prepara la consulta
  $dml = 'INSERT INTO Discapacidad (nombre, activo) VALUES (?,?)';
  if ( !($statement = $conexion_bd->prepare($dml)) ) {
      die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
      return 0;
  }
  //Unir los parámetros de la función con los parámetros de la consulta   
  //El primer argumento de bind_param es el formato de cada parámetro
  if (!$statement->bind_param("si", $nombre,$activo)) {
      die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
      return 0;
  } 
  //Executar la consulta
  if (!$statement->execute()) {
    die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
      return 0;
  }
  cerrar_bd($conexion_bd);
    return 1;
}


function editarDiscapacidad($Discapacidad_id, $nombre) {
  $conexion_bd = conectar_bd();
    
  //Prepara la consulta
  $dml = 'UPDATE Discapacidad SET nombre=(?) WHERE idDiscapacidad=(?)';
  if ( !($statement = $conexion_bd->prepare($dml)) ) {
      die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
      return 0;
  }
    
  //Unir los parámetros de la función con los parámetros de la consulta   
  //El primer argumento de bind_param es el formato de cada parámetro
  if (!$statement->bind_param("ss", $nombre, $Discapacidad_id)) {
      die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
      return 0;
  }
    
  //Executar la consulta
  if (!$statement->execute()) {
    die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
      return 0;
  }

  cerrar_bd($conexion_bd);
    return 1;
}
function eliminarDiscapacidad($discapacidad_id) {
  $conexion_bd = conectar_bd();
  //Prepara la consulta
  $dml = 'UPDATE Discapacidad SET Discapacidad.activo=0 WHERE Discapacidad.idDiscapacidad=(?)';
  if ( !($statement = $conexion_bd->prepare($dml)) ) {
      die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
      return 0;
  }
    
  //Unir los parámetros de la función con los parámetros de la consulta   
  //El primer argumento de bind_param es el formato de cada parámetro
  if (!$statement->bind_param("i",$discapacidad_id)) {
      die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
      return 0;
  }
    
  //Executar la consulta
  if (!$statement->execute()) {
    die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
      return 0;
  }

  cerrar_bd($conexion_bd);
    return 1;
}

function getAreas($nombre=""){
  $con = conectar_bd();
  $sql = "SELECT  A.idArea as idA, A.nombre as NombreA, A.activo as activo
  FROM Area as A ";
  if($nombre != ""){
      $sql .= " where     D.nombre like '%".$nombre."%'";
  }
  $result = mysqli_query($con, $sql);
  $tabla = "";
  if(mysqli_num_rows($result)){
      $tabla .= "<table class=\"highlight centered\">";
      $tabla .= "<thead><tr><th>Id de area</th><th>Nombre</th><th>Activo</th><th></th></thead>";
      while($row = mysqli_fetch_assoc($result)){   
          $tabla .= "<tr>";
          $tabla .= "<td>". $row["idA"]. "</td>";
          $tabla .= "<td>". $row["NombreA"]. "</td>";
          if($row["activo"]==1){
            $tabla .= "<td>"."<i class=\"material-icons\">check</i>". "</td>";
          }else{
            $tabla .= "<td>"."<i class=\"material-icons\">block</i>". "</td>";
          }
          $tabla .= '<td><a class="waves-effect waves-light btn-small" href="editarArea.php?area_id='.$row['idA'].'">'."<i class=\"material-icons\">create</i>"."</a>";
          $tabla .= '<a class="waves-effect waves-light btn-small" href="eliminarArea.php?area_id='.$row['idA'].'">'."<i class=\"material-icons\">delete_forever</i>"."</a>";
          $tabla .= "</td>"; 
          $tabla .= "</tr>";
      }
      $tabla .= "</table>";
  }
  else{
    $tabla .= "
    <div class=\"row\">
    <div class=\"col s12 m12 l12\">
        <div class=\"card blue lighten-1\">
            <div class=\"card-content white-text\">
                <span class=\"card-title\">No se encontró ningún resultado de ".$nombre."</span>
            </div>
        </div>
    </div>
</div>
    ";
  }
  cerrar_bd($con);
  
  return $tabla;
}

function getAreaById($id){
  $con = conectar_bd();
  
  $sql = "SELECT  A.idArea as idA, A.nombre as NombreA, A.activo as activo
  FROM Area as A ";
  $sql .= " Where A.idArea = '".$id."'";

  $result = mysqli_query($con, $sql);
  $resultado = "";
  
  
  while($row = mysqli_fetch_assoc($result)){
      $resultado .= "<br>
      <div class=\"card teal lighten-2\">
      <div class=\"card-content teal lighten-3\">
      <div class=\"carousel carousel-slider center\" data-indicators=\"true\">
        <div class=\"carousel-item teal lighten-5 \" href=\"#one!\" >
            <h2>Eliminar Area</h2>
          <div class=\"col s12 align-center\">
              <!-- Elemento -->
              <div class=\"file-field input-field\">
                <div class=\"col s12\">
                  <label>Nombre de la area</label>
                  <p>".$row["NombreA"]."</p>
              </div>
              </div>
              <br>
              <br>
              <br>
              <br>
              <div class=\"file-field input-field\">
                <div class=\"input-field col s12 l12 left\">
                  <a id=\"confirmarEliminarArea\" href=\"Controladores\Area\controladorEliminarArea.php?area_id=$id\" class=\"waves-effect red lime darken-4 btn right\">eliminar
                    <i class=\"material-icons right\">remove</i>
                  </a>
                  <a id=\"\" href=\"consultaArea.php\" class=\"waves-effect green lighten-3 btn right\">Volver<i class=\"material-icons right\">undo</i>
                  </a>
              </div>
              </div>

            </div>
        </div>
      </div>
      </div>
      </div>";
  }
  cerrar_bd($con);
  
  return $resultado;

}
function textArea($id=""){
  $con = conectar_bd();
      $sql = "SELECT  A.idArea as idA, A.nombre as NombreA
      FROM Area as A Where A.idArea = '".$id."'";
      $result = mysqli_query($con, $sql);

  $resultado = "<div class=\"file-field input-field\">
                          <div class=\"input-field col s12\">
                              <i class=\"material-icons prefix\"> </i>
                              <input placeholder=\"secundaria general 1\" type=\"text\"  name=\"area_nombre\" id=\"area_nombre\"
                              ";
      while($row = mysqli_fetch_assoc($result)){      
              $resultado .= " value=\"$row[NombreA]\"";
                  
      }
  $resultado .="
                  required>
                  <label for=\"rol\"><strong>Nombre</strong></label>     
                  </div>
                  </div>";
  cerrar_bd($con);
  return $resultado;

}
function insertarArea($nombre) {
  $conexion_bd = conectar_bd();
  $activo=1;
  //Prepara la consulta
  $dml = 'INSERT INTO Area (nombre, activo) VALUES (?,?)';
  if ( !($statement = $conexion_bd->prepare($dml)) ) {
      die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
      return 0;
  }
  //Unir los parámetros de la función con los parámetros de la consulta   
  //El primer argumento de bind_param es el formato de cada parámetro
  if (!$statement->bind_param("si", $nombre,$activo)) {
      die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
      return 0;
  } 
  //Executar la consulta
  if (!$statement->execute()) {
    die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
      return 0;
  }
  cerrar_bd($conexion_bd);
    return 1;
}


function editarArea($area_id, $nombre) {
  $conexion_bd = conectar_bd();
    
  //Prepara la consulta
  $dml = 'UPDATE Area SET nombre=(?) WHERE idArea=(?)';
  if ( !($statement = $conexion_bd->prepare($dml)) ) {
      die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
      return 0;
  }
    
  //Unir los parámetros de la función con los parámetros de la consulta   
  //El primer argumento de bind_param es el formato de cada parámetro
  if (!$statement->bind_param("ss", $nombre, $area_id)) {
      die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
      return 0;
  }
    
  //Executar la consulta
  if (!$statement->execute()) {
    die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
      return 0;
  }

  cerrar_bd($conexion_bd);
    return 1;
}
function eliminarArea($area_id) {
  $conexion_bd = conectar_bd();
  //Prepara la consulta
  $dml = 'UPDATE Area SET Area.activo=0 WHERE Area.idArea=(?)';
  if ( !($statement = $conexion_bd->prepare($dml)) ) {
      die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
      return 0;
  }
    
  //Unir los parámetros de la función con los parámetros de la consulta   
  //El primer argumento de bind_param es el formato de cada parámetro
  if (!$statement->bind_param("i",$area_id)) {
      die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
      return 0;
  }
    
  //Executar la consulta
  if (!$statement->execute()) {
    die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
      return 0;
  }

  cerrar_bd($conexion_bd);
    return 1;
}



function getEspecialidades($nombre=""){
  $con = conectar_bd();
  $sql = "SELECT  E.idEspecialidad as idE, A.idArea as idA,A.nombre AS NombreA ,E.nombre as NombreE, E.activo as activo
  FROM Area as A, Especialidad as E where E.idArea = A.idArea ";
  if($nombre != ""){
      $sql .= "    and E.nombre like '%".$nombre."%'";
  }
  $result = mysqli_query($con, $sql);
  $tabla = "";
  if(mysqli_num_rows($result)){
      $tabla .= "<table class=\"highlight centered\">";
      $tabla .= "<thead><tr><th>Id de Especialidad</th><th>Nombre Area</th><th>Nombre Especialidad</th><th>Activo</th><th></th></thead>";
      while($row = mysqli_fetch_assoc($result)){   
          $tabla .= "<tr>";
          $tabla .= "<td>". $row["idE"]. "</td>";
          $tabla .= "<td>". $row["NombreA"]. "</td>";
          $tabla .= "<td>". $row["NombreE"]. "</td>";
          if($row["activo"]==1){
            $tabla .= "<td>"."<i class=\"material-icons\">check</i>". "</td>";
          }else{
            $tabla .= "<td>"."<i class=\"material-icons\">block</i>". "</td>";
          }
          $tabla .= '<td><a class="waves-effect waves-light btn-small" href="editarEspecialidad.php?especialidad_id='.$row['idE'].'&&area_id='.$row['idA'].'">'."<i class=\"material-icons\">create</i>"."</a>";
          $tabla .= '<a class="waves-effect waves-light btn-small" href="eliminarEspecialidad.php?especialidad_id='.$row['idE'].'">'."<i class=\"material-icons\">delete_forever</i>"."</a>";
          $tabla .= "</td>"; 
          $tabla .= "</tr>";
      }
      $tabla .= "</table>";
  }
  else{
    $tabla .= "
    <div class=\"row\">
    <div class=\"col s12 m12 l12\">
        <div class=\"card blue lighten-1\">
            <div class=\"card-content white-text\">
                <span class=\"card-title\">No se encontró ningún resultado de ".$nombre."</span>
            </div>
        </div>
    </div>
</div>
    ";
  }
  cerrar_bd($con);
  
  return $tabla;
}

function getEspecialidadById($id){
  $con = conectar_bd();
  
  $sql = "SELECT  E.idEspecialidad as idE,A.nombre AS NombreA ,E.nombre as NombreE, E.activo as activo
  FROM Area as A, Especialidad as E ";
  $sql .= " Where E.idEspecialidad = '".$id."'";

  $result = mysqli_query($con, $sql);
  $resultado = "";
  
  
  while($row = mysqli_fetch_assoc($result)){
      $resultado .= "<br>
      <div class=\"card teal lighten-2\">
      <div class=\"card-content teal lighten-3\">
      <div class=\"carousel carousel-slider center\" data-indicators=\"true\">
        <div class=\"carousel-item teal lighten-5 \" href=\"#one!\" >
            <h2>Eliminar Especialidad</h2>
          <div class=\"col s12 align-center\">
              <!-- Elemento -->
              <div class=\"file-field input-field\">
                <div class=\"col s12\">
                  <label>Nombre de la Area</label>
                  <p>".$row["NombreA"]."</p>
                </div>
              </div>
              <br>
              <br>
              <!-- Elemento -->
              <div class=\"file-field input-field\">
                <div class=\"col s12\">
                  <label>Nombre de la especialidad</label>
                  <p>".$row["NombreE"]."</p>
                </div>
              </div>
              <br>
              <br>
              <br>
              <br>
              <div class=\"file-field input-field\">
                <div class=\"input-field col s12 l12 left\">
                  <a id=\"confirmarEliminarEspecialidad\" href=\"Controladores\Especialidad\controladorEliminarEspecialidad.php?especialidad_id=$id\" class=\"waves-effect red lime darken-4 btn right\">eliminar
                    <i class=\"material-icons right\">remove</i>
                  </a>
                  <a id=\"\" href=\"consultaEspecialidad.php\" class=\"waves-effect green lighten-3 btn right\">Volver<i class=\"material-icons right\">undo</i>
                  </a>
              </div>
              </div>

            </div>
        </div>
      </div>
      </div>
      </div>";
  }
  cerrar_bd($con);
  
  return $resultado;

}
function textEspecialidad($id=""){
  $con = conectar_bd();
      $sql = "SELECT  E.idEspecialidad as idE, E.nombre as NombreE
      FROM Especialidad as E Where E.idEspecialidad = '".$id."'";
      $result = mysqli_query($con, $sql);

  $resultado = "<div class=\"file-field input-field\">
                          <div class=\"input-field col s12\">
                              <i class=\"material-icons prefix\"> </i>
                              <input placeholder=\"secundaria general 1\" type=\"text\"  name=\"especialidad_nombre\" id=\"especialidad_nombre\"
                              ";
      while($row = mysqli_fetch_assoc($result)){      
              $resultado .= " value=\"$row[NombreE]\"";
                  
      }
  $resultado .="
                  required>
                  <label for=\"rol\"><strong>Nombre</strong></label>     
                  </div>
                  </div>";
  cerrar_bd($con);
  return $resultado;

}
function insertarEspecialidad($nombre,$area) {
  $conexion_bd = conectar_bd();
  $activo=1;
  //Prepara la consulta
  $dml = 'INSERT INTO Especialidad (nombre,idArea,activo) VALUES (?,?,?)';
  if ( !($statement = $conexion_bd->prepare($dml)) ) {
      die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
      return 0;
  }
  //Unir los parámetros de la función con los parámetros de la consulta   
  //El primer argumento de bind_param es el formato de cada parámetro
  if (!$statement->bind_param("sii", $nombre,$area,$activo)) {
      die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
      return 0;
  } 
  //Executar la consulta
  if (!$statement->execute()) {
    die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
      return 0;
  }
  cerrar_bd($conexion_bd);
    return 1;
}


function editarEspecialidad($especialidad_id,$area_id, $nombre) {
  $conexion_bd = conectar_bd();
    
  //Prepara la consulta
  $dml = 'UPDATE Especialidad SET nombre=(?),  idArea = (?)WHERE idEspecialidad=(?)';
  if ( !($statement = $conexion_bd->prepare($dml)) ) {
      die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
      return 0;
  }
    
  //Unir los parámetros de la función con los parámetros de la consulta   
  //El primer argumento de bind_param es el formato de cada parámetro
  if (!$statement->bind_param("sss", $nombre, $area_id,$especialidad_id)) {
      die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
      return 0;
  }
    
  //Executar la consulta
  if (!$statement->execute()) {
    die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
      return 0;
  }

  cerrar_bd($conexion_bd);
    return 1;
}
function eliminarEspecialidad($especialidad_id) {
  $conexion_bd = conectar_bd();
  //Prepara la consulta
  $dml = 'UPDATE Especialidad SET Especialidad.activo=0 WHERE Especialidad.idEspecialidad=(?)';
  if ( !($statement = $conexion_bd->prepare($dml)) ) {
      die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
      return 0;
  }
    
  //Unir los parámetros de la función con los parámetros de la consulta   
  //El primer argumento de bind_param es el formato de cada parámetro
  if (!$statement->bind_param("i",$especialidad_id)) {
      die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
      return 0;
  }
    
  //Executar la consulta
  if (!$statement->execute()) {
    die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
      return 0;
  }

  cerrar_bd($conexion_bd);
    return 1;
}

function crear_selectArea($seleccion=0) {
  $conexion_bd = conectar_bd();  
    
  $resultado = '<div class="input-field"><select name="area" required><option value="" disabled selected>Selecciona una opción</option>';
          
  $consulta = "SELECT 
  A.idArea as area_Id, A.nombre as area_nombre
  from Area as A ";
  $resultados = $conexion_bd->query($consulta);
  while ($row = mysqli_fetch_array($resultados, MYSQLI_BOTH)) {
      $resultado .= '<option value="'.$row["area_Id"].'"';
      if($seleccion == $row["area_Id"]) {
          $resultado .= 'selected';
      }
      $resultado .='>'.$row["area_Id"].'.-'.$row["area_nombre"].'</option>';

  }
      
  cerrar_bd($conexion_bd);
  $resultado .=  '</select><label>'.'beneficiaria'.'...</label></div>';
  return $resultado;
}
?>