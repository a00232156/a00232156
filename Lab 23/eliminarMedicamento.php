<?php
    session_start();
    require_once("util.php");
    include("Partials/General/_head.html");
    include("Partials/General/_topBar.html");
    include("Partials/General/_sideBar.html");
    include("Partials/General/_topBody.html");
    include("Partials/ConsultaEscolaridad/_fedback.html");
    include("Partials/ConsultaMedicamento/_consultaMedicamentoTitulo.html");

    $id=$_GET['medicamento_id'];
    if ( eliminarMedicamento($id)) {
        $_SESSION["mensaje"] = "Se ha eliminado el medicamento";
    } else {
        $_SESSION["warning"] = "Ocurrió un error al eliminar el medicamento";
    }
   // eliminarMedicamento($id);

    include("Partials/ConsultaMedicamento/_consultaMedicamento.html"); 
   

        showQueryMedicamentos(getMedicamentos());

    include("Partials/General/_endBody.html");
    include("Partials/General/_endPage.html");
?>