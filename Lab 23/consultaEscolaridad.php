<?php
    session_start();
    require_once("util.php");
    include("Partials/General/_head.html");
    include("Partials/General/_topBar.html");
    include("Partials/General/_sideBar.html");
    include("Partials/General/_topBody.html");
    include("Partials/ConsultaEscolaridad/_escolaridadTitulo.html");
    include("Partials/ConsultaEscolaridad/_fedback.html");
    
    echo "<div class=\"row\">";
        echo "<div class=\"col s12\">";
        include("Partials/ConsultaEscolaridad/_consultaEscolaridadHead.html");
        include("Partials/ConsultaEscolaridad/_consultaEscolaridad.html");    //cambio, para hacer nuestra tabla de consulta de programas dinamica debemos partir en 2 partials este archivo
        $escolaridad = "";
        
        echo getEscolaridad($escolaridad);
        
        include("Partials/ConsultaEscolaridad/_consultaEscolaridadFoot.html");
        echo "</div>";
    echo "</div>";


    include("Partials/General/_endBody.html");
    include("Partials/General/_endPage.html");
?>