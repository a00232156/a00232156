<?php
    session_start();
    require_once("util.php");
    include("Partials/General/_head.html");
    include("Partials/General/_topBar.html");
    include("Partials/General/_sideBar.html");
    include("Partials/General/_topBody.html");
    
    include("Partials/AltaEspecialidad/_altaEspecialidadTitulo.html");
    include("Partials/AltaEspecialidad/_altaEspecialidadFormulario.html");

    
    include("Partials/General/_endBody.html");
    include("Partials/General/_endPage.html");
?>