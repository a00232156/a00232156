<?php
    session_start();
    require_once("util.php");
    include("Partials/General/_head.html");
    include("Partials/General/_topBar.html");
    include("Partials/General/_sideBar.html");
    include("Partials/General/_topBody.html");
    include("Partials/ConsultaPrograma/_consultaProgramaTitulo.html");
        echo "<div class=\"row\">";
            echo "<div class=\"col s12\">";
            include("Partials/ConsultaPrograma/_consultaProgramaHead.html");
            include("Partials/ConsultaPrograma/_consultaPrograma.html");    //cambio, para hacer nuestra tabla de consulta de programas dinamica debemos partir en 2 partials este archivo
            $nombre = "";
            
            echo getProgramas($nombre);
            
            include("Partials/ConsultaPrograma/_consultaProgramaFoot.html");
            echo "</div>";
        echo "</div>";
    include("Partials/General/_endBody.html");
    include("Partials/General/_endPage.html");
?>