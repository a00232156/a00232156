<?php
    require_once("util.php");
    include("Partials/General/_head.html");
    include("Partials/General/_topBar.html");
    include("Partials/General/_sideBar.html");
    include("Partials/General/_topBody.html");
    include("Partials/EditarEspecialidad/_editarEspecialidadTitulo.html");
    include("Partials/EditarEspecialidad/_editarEspecialidadFormularioHead.html");
        $especialidad_id = htmlspecialchars($_GET["especialidad_id"]);
        $area_id = htmlspecialchars($_GET["area_id"]);
        echo"<form action=\"Controladores\Especialidad\controladorEditarEspecialidad.php?especialidad_id=$especialidad_id\" method=\"post\">";
        echo textEspecialidad($especialidad_id);
        echo "
        <div class=\"file-field input-field\">
        <div class=\"input-field col s12\">
          <i class=\"material-icons prefix\"> </i>
            ".crear_selectArea($area_id)." 
        </div>
      </div> ";
    include("Partials/EditarEspecialidad/_editarEspecialidadFormularioFoot.html");
    include("Partials/General/_endBody.html");
    include("Partials/General/_endPage.html");
?>  