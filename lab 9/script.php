<?php
  function promedio($A) {
    $suma = 0;
    foreach ($A as $value) {
      $suma = $suma + $value ;
    }
    return ($suma/sizeof($A));
  }

  function mediana($A){
    sort($A);
    return $A[sizeof($A)/2];
  }

  function muestraArreglo($A){
    $string = "[";
    foreach ($A as $value){
      $string = $string . $value . ", ";
    }
    $string = $string . "]";
    return $string;
  }
  function media($A){
    sort($A);
    return (($A[sizeof($A)] + $A[0])/2);
  }

  function muestraTablaN($n){
    $string = "<table class=\"hover\" class= \"stack\">
      <tr>
        <th>Numero</th>
        <th>Cuadrado</th> 
        <th>Cubo</th>
      </tr>";
    for($i = 1; $i<=$n;$i++){
      $string = $string . "<tr><td> ".$i."</td>"."<td> ".$i*$i."</td>"."<td> ".$i*$i*$i."</td></tr>";
    }
    $string."</table>";
    return $string;
  }
?>