<?php
    session_start();
    require_once("util.php");
    
    $idZombie = htmlspecialchars($_SESSION["idZombie"]);
    $idEstado = htmlspecialchars($_POST["Estado"]);
    
    if((isset($_POST["Estado"])) and (isset($_SESSION["idZombie"]))) {
        if (nuevoEstadoZombi($idZombie,$idEstado)) {
            $_SESSION["mensaje"] = "Se ha modificado al zombi";
        } else {
            $_SESSION["warning"] = "Ocurrió un error al modificar al zombi";
        }
    }

    header("location:index.php");
?>