<?php
function conectar_bd() {

    $conexion_bd = mysqli_connect("mysql1008.mochahost.com","dawbdorg_0232156","0232156","dawbdorg_A00232156");

    if ($conexion_bd == NULL) {
        
        die("No se pudo conectar con la base de datos");
    }
    
    return $conexion_bd;
}


function cerrar_bd($mysql){
    mysqli_close($mysql);
}


function Zombis(){
    $con = conectar_bd();
    
    $sql = "SELECT ZE.idZombie as idZombie, Z.nombre as nombreZ
    from Zombie as Z,ZombieTieneEstado as ZE, Estado as E
    where Z.idZombie = ZE.idZombie
    and ZE.idEstado = E.idEstado group by ZE.idZombie";
    $result = mysqli_query($con, $sql);
    $tabla = "";
    
    if(!(mysqli_num_rows($result) == 0)){
      $tabla .= "<table class=\"highlight\">
      <thead>
          <tr>
              <th>Zombie</th>
              <th>Estados</th>
          </tr>
      </thead>
      <tbody>";
        while($row = mysqli_fetch_assoc($result)){   
            $tabla .= "<tbody><tr><td>". $row["nombreZ"]."</td>";
            $tabla .= getEstadosId($row["idZombie"]);
            $tabla .= "</tr>";
        }
        $tabla .= "</tbody></table>";
    }
    
    cerrar_bd($con);
    
    return $tabla;
  }

function getEstadosId($idZombie){
    $con = conectar_bd();
    
    $sql = "SELECT ze.Fecha as Fecha, e.nombre as nombreE
    from ZombieTieneEstado as ze, Estado as e
    where e.idEstado = ze.idEstado
    and ze.idZombie = $idZombie";
    $result = mysqli_query($con, $sql);
    $tabla = "";
    
    if(!(mysqli_num_rows($result) == 0)){
      $tabla .= "<td>";
      while($row = mysqli_fetch_assoc($result)){   
        $tabla .= $row["nombreE"];
        $tabla .= " (".$row["Fecha"]. ")";
        $tabla .= "<br>";     
    }
    $tabla .= "<a class=\"waves-effect waves-light btn\" href=\"registrarEstado.php?idZombie=$idZombie\"><i class=\"material-icons left\">add</i>Registrar estado</a>";
      $tabla .= "</td>";
    }
    cerrar_bd($con);
    
    return $tabla;
}


function registroZombi($nombre){
    $conexion_bd = conectar_bd();
      
    //Prepara la consulta
    $dml = 'INSERT INTO Zombie (nombre) VALUES (?)';
    $dmlF = 'INSERT INTO  ZombieTieneEstado  (idZombie,idEstado)
    values (LAST_INSERT_ID(),\'1\')'; 
    if ( !($statement = $conexion_bd->prepare($dml)) ) {
        die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
        return 0;
    }
      
    //Unir los parámetros de la función con los parámetros de la consulta   
    //El primer argumento de bind_param es el formato de cada parámetro
    if (!$statement->bind_param("s",$nombre)) {
        die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }
      
    //Executar la consulta
    if (!$statement->execute()) {
      die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }
    if(!$conexion_bd->query($dmlF)){
        return 1;
    }
    
    cerrar_bd($conexion_bd);
      return 1;



}

function estadoEdit($id){
    $con = conectar_bd();

    $sql = "SELECT nombre 
    from Zombie
    where idZombie = $id";

    $result = mysqli_query($con, $sql);

    $consulta = "";


    if(!(mysqli_num_rows($result) == 0)){
        $consulta .= "<div class=\"container\">
        <h3>Registrar nuevo estado de zombi</h3>
    <div class=\"row\">
        <form action=\"controladorEditar.php\" method=\"POST\">
        <div class=\"input-field col s6\">";
        while($row = mysqli_fetch_assoc($result)){   
          $consulta .= "<div class=\"input-field col s6\">";
          $consulta .= "<p>".$row["nombre"]."</p>";
          $consulta .= "<label class=\"active\" for=\"first_name2\">Nombre</label>
        </div>";
    }
        $consulta .="<div class=\"input-field col s6\">";
        $consulta .= selectEstados();
        $consulta .= "</div>
        </div>
        <div class=\"row\">
            <div class=\"col s8\"></div>
            <div class=\" col s2 \"> <button type=\"submit\" class=\"waves-effect waves-light btn\" >Registrar</button></div>
        </div>
    </form>
    </div>";   

      $consulta .= "</div>";
      }
      cerrar_bd($con);
      
      return $consulta;
}


function selectEstados(){
    $con = conectar_bd();
    
    $sql = "SELECT idEstado as id, nombre FROM Estado";
    
    $result = mysqli_query($con, $sql);
    $select = "";
    
    if(mysqli_num_rows($result)){
        $select .= "<select class=\"browser-default\"name=\"Estado\" id=\"Estado\" required >";

       $select .= "<option value=\"\" selected >Elija un estado</option>";

        while($row = mysqli_fetch_assoc($result)){   
            $select .= '<option value="'.$row["id"].'">'.$row["nombre"].'</option>';
        }
        $select .= "</select>";
        
    }
    cerrar_bd($con);
    
    return $select;
}

function nuevoEstadoZombi($idZombie,$idEstado){
    
    $conexion_bd = conectar_bd();
      
    //Prepara la consulta
    $dml = 'INSERT INTO ZombieTieneEstado (`idZombie`,`idEstado`) values (?,?)';
    if ( !($statement = $conexion_bd->prepare($dml)) ) {
        die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
        return 0;
    }
      
    //Unir los parámetros de la función con los parámetros de la consulta   
    //El primer argumento de bind_param es el formato de cada parámetro
    if (!$statement->bind_param("ss", $idZombie,$idEstado)) {
        die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }
      
    //Executar la consulta
    if (!$statement->execute()) {
      die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }

    cerrar_bd($conexion_bd);
      return 1;
  }

?>