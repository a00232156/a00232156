<?php
    session_start();
    require_once("util.php");
    
    $nombre = htmlspecialchars($_POST["nombre"]);
    
    if((isset($_POST["nombre"]))) {
        if (registroZombi($nombre)) {
            $_SESSION["mensaje"] = "Se ha agregado al zombi";
        } else {
            $_SESSION["warning"] = "Ocurrió un error al agregar al zombi";
        }
    }

    header("location:registroZombi.php");

?>