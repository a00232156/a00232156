CREATE TABLE Materiales(
  Clave numeric(5),
  Descripcion varchar(50),
  Costo numeric(8,2)
);

CREATE TABLE proyectos(
  Numero numeric(5),
  Denominacion varchar(50)
);

CREATE TABLE proveedores(
  RFC numeric(5),
  RazonSocial varchar(50)
);

CREATE TABLE entregan(
  Clave numeric(5),
  RFC numeric(5),
  Numero numeric(5),
  Fecha DATETIME,
  Cantidad numeric(8,2)
);