DROP TABLE IF EXISTS Materiales;

CREATE TABLE Materiales
(
  Clave int not null AUTO_INCREMENT,
  Descripcion varchar(50),
  Costo numeric (8,2),
  primary key (Clave)
);

DROP TABLE IF EXISTS Proveedores;

CREATE TABLE Proveedores
(
  RFC char(13) not null,
  RazonSocial varchar(50),
  primary key (RFC)
);

DROP TABLE IF EXISTS Proyectos;

CREATE TABLE Proyectos
(
  Numero int not null AUTO_INCREMENT,
  Denominacion varchar(50),
  primary key (Numero)
);

DROP TABLE IF EXISTS Entregan;

CREATE TABLE Entregan
(
  Clave numeric(5) not null,
  RFC char(13) not null,
  Numero numeric(5) not null,
  Fecha DateTime not null,
  Cantidad numeric (8,2)
);
