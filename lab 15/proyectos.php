<?php

    include("_header.html");
    require_once("util.php");

    $result = getProyectos();
    
    if(mysqli_num_rows($result)){
        echo "<table class=\"striped centered\">";
        echo "<thead><tr><th>Numero de Proyecto</th><th>Denominacion</th></tr></thead>";
        while($row = mysqli_fetch_assoc($result)){   
            echo "<tr>";
            echo "<td>". $row["Numero"]. "</td>";
            echo "<td>". $row["Denominacion"]. "</td>";
            echo "</tr>";
        }
        echo "</table>";
    }
    
    include("_footer.html");
?>