<?php
//función para conectarnos a la BD

function conectar_bd() {

    $conexion_bd = mysqli_connect("localhost","root","","lab15");

    if ($conexion_bd == NULL) {
        
        die("No se pudo conectar con la base de datos");
    }
    
    return $conexion_bd;
}

//funcion para cerrar la base de datos.
function cerrar_bd($mysql){
    mysqli_close($mysql);
}

function getMateriales(){
    $con = conectar_bd();
    
    $sql = "SELECT * FROM Materiales";
    
    $result = mysqli_query($con, $sql);
    
    cerrar_bd($con);
    
    return $result;
    
}
function getProyectos(){
    $con = conectar_bd();
    
    $sql = "SELECT * FROM Proyectos";
    
    $result = mysqli_query($con, $sql);
    
    cerrar_bd($con);
    
    return $result;
    
}

function insertMateriales($descripcion, $costo){
    $conexion_bd = conectar_bd();
      
    //Prepara la insercion
    $dml = 'INSERT INTO materiales (Descripcion,Costo) VALUES (?,?)';
    if ( !($statement = $conexion_bd->prepare($dml)) ) {
        die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
        return 0;
    }
      
    //Unir los parámetros de la función con los parámetros de la consulta   
    //El primer argumento de bind_param es el formato de cada parámetro
    if (!$statement->bind_param("ss", $descripcion, $costo)) {
        die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }
      
    //Executar la insercion
    if (!$statement->execute()) {
      die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }

    cerrar_bd($conexion_bd);
      return 1;
}



function delProy($Numero){
    $conexion_bd = conectar_bd();
      
    //Prepara la insercion
    $dml = 'DELETE From Proyectos Where Numero = (?)';
    if ( !($statement = $conexion_bd->prepare($dml)) ) {
        die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
        return 0;
    }
      
    //Unir los parámetros de la función con los parámetros de la consulta   
    //El primer argumento de bind_param es el formato de cada parámetro
    if (!$statement->bind_param("s", $Numero)) {
        die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }
      
    //Executar la insercion
    if (!$statement->execute()) {
      die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }

    cerrar_bd($conexion_bd);
      return 1;
}

function updateProyById($Numero,$Denominacion){
    $conexion_bd = conectar_bd();
    
    $dml = "UPDATE Proyectos SET Denominacion='".$Denominacion."' WHERE Numero='".$Numero."'";
    
    $result = mysqli_query($conexion_bd,$dml);
    
    cerrar_bd($conexion_bd);
      return 1;
    
    return $result;
}


?>