<?php
  session_start();
  require_once("util.php");  

  $_POST["Numero"] = htmlspecialchars($_POST["Numero"]);


  if((isset($_POST["Numero"]))){
      if (delProy($_POST["Numero"])) {
          $_SESSION["mensaje"] = "Se eliminó el proyecto";
      }else {
          $_SESSION["warning"] = "Ocurrió un error al eliminar el proyecto";
      }
  }

  header("location:index.php");
?>