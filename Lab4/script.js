function problema1(){
    let number = prompt("Dame un numero");
    document.write("<table>");
    for(i = 1; i <= number; i++){
        cuadrado = i*i;
        cubo = i*i*i;
        document.write("<tr>Numero:"+i+"  Cuadrado: "+cuadrado+"  Cubo:"+cubo+"</tr><br>");
    }
    document.write("</table>");
}

function problema2(){
  let respuestaUser = prompt("Adivina el resultado de la suma de 2 numeros aleatorios entre 1-20.");
  let sumando1 = Math.floor(Math.random() * 20) + 1;
  let sumando2 = Math.floor(Math.random() * 20) + 1;
  let suma = sumando1 + sumando2;
  let tiempoinicial = performance.now();
  if(suma==respuestaUser){
    let tiempofinal = performance.now();
    let tiempototal = ((tiempofinal-tiempoinicial)/1000);
    alert("¡Felicidades! Usted ha acertado, los numeros eran: "+sumando1+" y "+sumando2);
    alert("Te has tardado en responder"+tiempototal);
  }else{
    let tiempofinal = performance.now();
    let tiempototal = ((tiempofinal-tiempoinicial)/1000);
    alert("¡Qué mal! Usted se ha equivocado, los numeros eran: "+sumando1+" y "+sumando2);
    alert("Te has tardado en responder "+tiempototal+ "segundos");
  }
}

function valorarr(){
  let arr = [];
  tamano = prompt("Introduce el tamano del arreglo");
  for(let i = 0; i < tamano; i++){
    arr.push(prompt("Introduzca un numero positivo o negativo, o algun cero"));
  }
  problema3(arr);
}
function problema3(arr) {
  limite = arr.length;
  positivos = 0;
  zeros = 0;
  negativos = 0;
  for(let i=0; i<limite; i++){
    if(arr[i] < 0){
      negativos++;
    }
    if(arr[i] < 0){
      negativos++;
    }
    if(arr[i] > 0){
      positivos++;
    }
    if(arr[i] == 0){
      zeros++;
    }
  }
  document.write("<p> Numeros negativos: "+negativos+"<p><br>"+"<p> Numeros con valor cero: "+zeros+"<p><br>"+"<p> Numeros positivos: "+positivos+"<p><br>");
}

function problema4(arr) {
let matrix = new Array(3);
  for(let i=0; i<3; i++) {
    matrix[i] = [];
    for(let j=0; j<3; j++) {
        matrix[i][j] = prompt("introduzca el valor de la posición (" +i+","+j+")");
    }
  }
  for(let i = 0; i < 3; i++){
    let suma = 0;
    for(let j = 0; j< 3; j++){
      suma += +matrix[i][j];
    }
    let promedio = suma/3;
    alert("El promedio de la fila #"+i+" es: "+promedio);
  }
}

function problema5(){
  let numero = prompt("Dame un numero a revertir");
  let stringdiv = numero.split("");
  let reverso = stringdiv.reverse();
  let unir = reverso.join("");
  alert("Tu numero invertido es: "+unir);
}

function problema6(){
  //tablas de multiplicar
  let numero = prompt("¿Qué tabla de multiplicar desea?");
  let limite = prompt("¿Hasta Qué múltiplo?");
  let tablita = new tabla(numero, limite);
  tablita.imprimir();
}

function tabla(nuevo_numero, nuevo_limite) {
  this.numero = nuevo_numero;
  this.limite = nuevo_limite;
  this.imprimir = imprimir_tabla;
}

function imprimir_tabla(){
  document.write("<table>");
  for(let i=1; i <= this.limite; i++){
    document.write("<tr>1 x "+i+": "+(i*+this.numero)+"</tr><br>");
  }
  document.write("</table>");
}

function imprimir_exito(){
  alert("Su tabla del numero "+this.numero+" hasta el limite: "+this.limite+" ha sido impresa con exito gracias por usar esta pagina web!");
}