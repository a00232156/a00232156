<?php
    
    include("_header.html");
    require_once("util.php");

    $result = getMateriales();
    include("_formBuscarMateriales.html");
    include("_resultadoConsultaM.html");

    if(mysqli_num_rows($result)){
        echo "<table class=\"striped centered\">";
        echo "<thead><tr><th>Clave</th><th>Descripcion</th><th>Precio</th></tr></thead>";
        while($row = mysqli_fetch_assoc($result)){   
            echo "<tr>";
            echo "<td>". $row["Clave"]. "</td>";
            echo "<td>". $row["Descripcion"]. "</td>";
            echo "<td>". $row["Costo"]. "</td>";
            echo "</tr>";
        }
        echo "</table>";
    }
    
    include("_footer.html");
    
?>