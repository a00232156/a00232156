<?php
  session_start();
  require_once("util.php");  

  $_POST["Descripcion"] = htmlspecialchars($_POST["Descripcion"]);
  $_POST["Precio"] = htmlspecialchars($_POST["Precio"]);

  if((isset($_POST["Descripcion"])) and (isset($_POST["Precio"]))  ) {
      if (insertMateriales($_POST["Descripcion"],$_POST["Precio"])) {
          $_SESSION["mensaje"] = "Se registró el material";
      } else {
          $_SESSION["warning"] = "Ocurrió un error al registrar el material";
      }
  }

  header("location:index.php");
?>