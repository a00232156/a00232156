//Función para crear el objeto para realizar una petición asíncrona
function getRequestObject() {
    // Asynchronous objec created, handles browser DOM differences
    if (window.XMLHttpRequest) {
        // Mozilla, Opera, Safari, Chrome IE 7+
        return (new XMLHttpRequest());
    } else if (window.ActiveXObject) {
        // IE 6-
        return (new ActiveXObject("Microsoft.XMLHTTP"));
    } else {
        // Non AJAX browsers
        return (null);
    }
}


//Función que detonará la petición asíncrona
function buscar_old() {
    request = getRequestObject();
    if (request != null) {
        let Numero = document.getElementById("Numero").value;
        var url = 'controladorProyectos.php?Numero=' + Numero ;
        
        request.open('GET', url, true);
        request.onreadystatechange =
            function () {
                if ((request.readyState == 4)) {
                    // Se recibió la respuesta asíncrona, entonces hay que actualizar el cliente.
                    // A esta parte comúnmente se le conoce como la función del callback
                    document.getElementById("consultaProyectos").innerHTML = request.responseText;
                }
            };
        // Limpiar la petición
        request.send(null);

    }
}

function buscar() {
    //$.post manda la petición asíncrona por el método post. También existe $.get
    $.post("controladorProyectos.php", {
        Numero: document.getElementById("Numero").value
        }).done(function (data) {
        $("#consultaProyectos").html(data);
    });
}

function buscarM() {
    //$.post manda la petición asíncrona por el método post. También existe $.get
    $.post("controladorMateriales.php", {
        Clave: document.getElementById("Clave").value
        }).done(function (data) {
        $("#consultaMateriales").html(data);
    });
}

//si nuestro elemento existe entonces deberíamos 
//Asignar al botón buscar, la función buscar() correspondiente
if ($('#infoMateriales').length > 0) {
    document.getElementById("infoMateriales").onclick = buscarM;
  }

if ($('#infoProyectos').length > 0) {
    document.getElementById("infoProyectos").onclick = buscar;
}