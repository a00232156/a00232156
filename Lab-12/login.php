<?php 
  session_start();
  if ( !(isset($_POST["nombre"]) && isset($_POST["password"]))) {
    die();
  }
  $_SESSION["nombre"] = htmlspecialchars($_POST["nombre"]);
  $_SESSION["password"] = htmlspecialchars($_POST["password"]);
  include("_header.html");
  include("_welcome.html");
  include("_footer.html"); 
?> 