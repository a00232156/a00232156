<?php
  session_start();
  require_once("util.php");  

  $_POST["Numero"] = htmlspecialchars($_POST["Numero"]);
  $_POST["Denominacion"] = htmlspecialchars($_POST["Denominacion"]);
    
  if(strlen($_POST["Numero"]) > 0 and strlen($_POST["Denominacion"]) > 0 ){
     if((isset($_POST["Numero"])) and (isset($_POST["Denominacion"]))){
          if (updateProyById($_POST["Numero"],$_POST["Denominacion"])) {
              $_SESSION["mensaje"] = "Se actualizó el proyecto";
          }else {
              $_SESSION["warning"] = "Ocurrió un error al actualizar el proyecto";
          }
      } 
  }
  header("location:index.php");
?>