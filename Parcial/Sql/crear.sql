SET FOREIGN_KEY_CHECKS=0;

/*Temporal drop table para testear la creacion de las tablas para la base de datos*/
DROP TABLE IF EXISTS  Tipo;
DROP TABLE IF EXISTS  Lugar;
DROP TABLE IF EXISTS  Incidente;

CREATE TABLE `Tipo`(
	`idTipo` int (11) not null,
	`nombre` varchar(40)
);
CREATE TABLE `Lugar`
(
	`idLugar` int(11) not null,
	`nombre` varchar(40)
);

CREATE TABLE `Incidente`
(
	`idTipo`  int (11) not null,
    `idLugar` int(11) not null,
	`Fecha` datetime default current_timestamp
);

/*Primary keys de nuestras tablas y Constraints de nuestras tablas y auto increment*/

ALTER TABLE `Tipo` 
  ADD PRIMARY KEY (`idTipo`),
  MODIFY `idTipo` int(11) NOT NULL AUTO_INCREMENT
  ;
  ALTER TABLE `Lugar` 
  ADD PRIMARY KEY (`idLugar`),
  MODIFY `idLugar` int(11) NOT NULL AUTO_INCREMENT;
  ;
  ALTER TABLE `Incidente`
	ADD PRIMARY KEY (`idTipo`,`idLugar`,`Fecha`),
	ADD CONSTRAINT `Tipo` FOREIGN KEY (`idTipo`) REFERENCES `Tipo` (`idTipo`),
    ADD CONSTRAINT `Lugar` FOREIGN KEY (`idLugar`) REFERENCES `Lugar` (`idLugar`)
  ;
  
 
INSERT INTO `Tipo` (`nombre`)
VALUE ('Falla eléctrica'),
('Fuga de herbívoro'),
('Fuga de Velociraptors'),
('Fuga de TRex'),
('Robo de ADN'),
('Auto descompuesto'),
('Visitantes en zona no autorizada'); 
 
 
INSERT INTO `Lugar` (`nombre`)
VALUE ('Centro turístico'),
('Laboratorios'),
('Restaurante'),
('Centro operativo'),
('Triceratops'),
('Dilofosaurios'),
('Velociraptors'),
('TRex'),
('Planicie de los herbívoros'); 
 
 insert into `incidente` (`idlugar`, `idtipo`)
 values ('1','1');
 

DROP PROCEDURE IF EXISTS creaIncidente;
DELIMITER //
CREATE PROCEDURE creaIncidente (
	IN p_uidlugar int(11),
	IN p_uidtipo int(11)
)
	BEGIN
		insert into incidente (idlugar,idtipo) values (p_uidlugar,p_uidtipo);
	END//
DELIMITER ;

 SET FOREIGN_KEY_CHECKS=1;
 
