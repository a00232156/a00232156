function buscar() {
    //$.post manda la petición asíncrona por el método post. También existe $.get
    console.log("hola");
    $.post("controladorConsulta.php", {
        Nombre: document.getElementById("Incidente").value
        }).done(function (data) {
        $("#resultado").html(data);
    });
}


//Si nuestro elemento existe entonces deberíamos 
//Asignar al botón buscar, la función buscar() correspondiente
if ($('#Incidente').length > 0) {
    document.getElementById("Incidente").onkeyup = buscar;
}