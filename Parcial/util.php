<?php
function conectar_bd() {

    $conexion_bd = mysqli_connect("mysql1008.mochahost.com","dawbdorg_0232156","0232156","dawbdorg_A00232156");
    $conexion_bd->set_charset("utf8");
    if ($conexion_bd == NULL) {
        
        die("No se pudo conectar con la base de datos");
    }
    
    return $conexion_bd;
}


function cerrar_bd($mysql){
    mysqli_close($mysql);
}

function incidentes($nombre=""){
    $con = conectar_bd();
    $sql = "SELECT L.nombre as LugarNombre, T.nombre as TipoNombre, I.fecha 
    from Lugar as L, Incidente as I, Tipo as T
    where L.idLugar = I.idLugar
    and I.idTipo = T.idTipo";
    if($nombre != ""){
        $sql .= " AND T.nombre like '%".$nombre."%'";
    }
    
    $sql .= " order by I.fecha desc";
    
    
    $result = mysqli_query($con, $sql);
    
    $tabla = "";
    
    if(mysqli_num_rows($result)){
      $tabla .= "
      <table class=\"highlight\">
      <thead>
          <tr>
              <th>Tipo de Incidente</th>
              <th>Lugar del incidente</th>
              <th>Fecha</th>
          </tr>
      </thead>
      <tbody>";
        while($row = mysqli_fetch_assoc($result)){   
            $tabla .= "<tbody><tr><td>". $row["TipoNombre"]."</td>";
            $tabla .= "<td>". $row["LugarNombre"]."</td>";
            $tabla .= "<td>". $row["fecha"]."</td>";
            $tabla .= "</tr>";
        }
        $tabla .= "</tbody></table>";
    }
    
    cerrar_bd($con);
    
    return $tabla;
}


function registro($idlugar, $idtipo){
    $conexion_bd = conectar_bd();
      
    //Prepara la consulta
    $dml = 'CALL creaIncidente(?,?)';
    if ( !($statement = $conexion_bd->prepare($dml)) ) {
        die("Error: (" . $conexion_bd->errno . ") " . $conexion_bd->error);
        return 0;
    }
      
    //Unir los parámetros de la función con los parámetros de la consulta   
    //El primer argumento de bind_param es el formato de cada parámetro
    if (!$statement->bind_param("ss",$idlugar, $idtipo)) {
        die("Error en vinculación: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }
      
    //Executar la consulta
    if (!$statement->execute()) {
      die("Error en ejecución: (" . $statement->errno . ") " . $statement->error);
        return 0;
    }

    
    cerrar_bd($conexion_bd);
      return 1;

}


function selectTipo(){
    $con = conectar_bd();
    
    $sql = "SELECT idTipo as id, nombre FROM Tipo";
    
    $result = mysqli_query($con, $sql);
    $select = "";
    
    if(mysqli_num_rows($result)){
        $select .= "<select class=\"browser-default\"name=\"Tipo\" id=\"Tipo\" required >";

       $select .= "<option value=\"\" selected >Elija un Tipo</option>";

        while($row = mysqli_fetch_assoc($result)){   
            $select .= '<option value="'.$row["id"].'">'.$row["nombre"].'</option>';
        }
        $select .= "</select>";
        
    }
    cerrar_bd($con);
    
    return $select;
}

function selectLugar(){
    $con = conectar_bd();
    
    $sql = "SELECT idLugar as id, nombre FROM Lugar";
    
    $result = mysqli_query($con, $sql);
    $select = "";
    
    if(mysqli_num_rows($result)){
        $select .= "<select class=\"browser-default\"name=\"Lugar\" id=\"Lugar\" required >";

       $select .= "<option value=\"\" selected >Elija un Lugar</option>";

        while($row = mysqli_fetch_assoc($result)){   
            $select .= '<option value="'.$row["id"].'">'.$row["nombre"].'</option>';
        }
        $select .= "</select>";
        
    }
    cerrar_bd($con);
    
    return $select;
}





?>