<?php
    session_start();
    require_once("util.php");
    
    $lugar = htmlspecialchars($_POST["Lugar"]);
    $Tipo = htmlspecialchars($_POST["Tipo"]);
    
    if((isset($_POST["Lugar"])) and (isset($_POST["Tipo"]))) {
        if (registro($lugar,$Tipo)) {
            $_SESSION["mensaje"] = "Se ha agregado el incidente";
        } else {
            $_SESSION["warning"] = "Ocurrió un error al agregar el incidente";
        }
    }

    header("location:registro.php");

?>